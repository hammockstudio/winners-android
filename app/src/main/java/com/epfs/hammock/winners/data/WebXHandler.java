package com.epfs.hammock.winners.data;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.helpers.Commons;

import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;

import javax.crypto.Cipher;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by allan on 8/9/17.
 */

public class WebXHandler {
    private static String PUBLIC_KEY =
            "-----BEGIN PUBLIC KEY-----" +
                    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDKpb4Gq9tWMWkYuWV8Zgf3bzgV" +
                    "BvxtWzSZzHpFgEgVGJ/4iHIJuy5o4QsJTD80l8UI3WyIRjJ9cqEOokl7RHdcYxkh" +
                    "GhdIebUDnCalfSDB18uf3Ki7pku9sfzWit7P7b1e8OV6GcNEVw4soRBb1w3ABAdK" +
                    "/ay8A3c4M7KGTXki7QIDAQAB" +
                    "-----END PUBLIC KEY-----";
    private static String secret_key = "a806c75a-9924-49e7-977b-188a41e43be5";

    OkHttpClient client = new OkHttpClient();


    public byte[] encryptWithPublicKey(String encrypt) throws Exception {
        byte[] message = encrypt.getBytes("UTF-8");
        PublicKey apiPublicKey = getRSAPublicKeyFromString();
        Cipher rsaCipher = Cipher.getInstance("RSA");
        rsaCipher.init(Cipher.ENCRYPT_MODE, apiPublicKey);
        return rsaCipher.doFinal(message);
    }

    private PublicKey getRSAPublicKeyFromString() throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] publicKeyBytes = Base64.decode(PUBLIC_KEY.getBytes("UTF-8"), Base64.NO_WRAP);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(publicKeyBytes);
        return keyFactory.generatePublic(x509KeySpec);
    }

    public void sendPost(Context context, String amount, HashMap<String, String> params) {
        String url = context.getResources().getString(R.string.webx_url);
        Security.addProvider(new BouncyCastleProvider());

        String encrypted = "";
        try {

            encrypted = Base64.encodeToString(encryptWithPublicKey("123123|10000"), Base64.NO_WRAP);
        } catch (Exception e) {
            Log.d("WEBX Public Key", "sendPost: " + PUBLIC_KEY);
            e.printStackTrace();
        }
//

        RequestBody formBody = new FormBody.Builder()
                .add("first_name", "steve")
                .add("last_name", "peiries")
                .add("email", "steve.peiries@gmail.com")
                .add("contact_number", "0775135986")
                .add("address_line_one", "someValue")
                .add("city", "someValue")
                .add("state", "someValue")
                .add("postal_code", "10280")
                .add("country", "someValue")
                .add("secret_key", secret_key)
                .add("payment", encrypted)
                .build();



        post(url, formBody, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // Something went wrong
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseStr = response.body().string();
                    Log.d("WEBX RESPONSE:", "onResponse: " + responseStr);
                    // Do what you want to do with the response.
                } else {
                    // Request not successful
                }
            }
        });

    }

    Call post(String url, RequestBody requestBody, Callback callback) {
//        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }
}

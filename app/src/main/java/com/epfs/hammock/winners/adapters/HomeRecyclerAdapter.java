package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.activities.CategoryActivity;
import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.models.HomeItem;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> implements View.OnClickListener {
    private List<HomeItem> items;
    private Context mContext;

    @Override
    public void onClick(View view) {
        mContext.startActivity(new Intent(mContext, CategoryActivity.class));
    }

    public void setData(List<HomeItem> items){
        this.items = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        private TextView txtMainItemName;
        private RecyclerView homeList;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtMainItemName = (TextView) itemView.findViewById(R.id.txtMainItemName);
            homeList = (RecyclerView) itemView.findViewById(R.id.homepage_item_list);
            //homepage_item_list
        }
    }

    public HomeRecyclerAdapter(List<HomeItem> items, Context context) {
        this.items = items;
        this.mContext = context;
    }

    @Override
    public HomeRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listitem_home_item, parent, false);
        v.setOnClickListener(this);

        return new HomeRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HomeRecyclerAdapter.ViewHolder holder, int position) {
        HomeItem item = items.get(position);
        holder.txtMainItemName.setText(item.getName());

        HomeItemRecyclerAdapter homeItemRecyclerAdapter = new HomeItemRecyclerAdapter(item.getHomeListItems(), mContext);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.homeList.setLayoutManager(mLayoutManager);
        holder.homeList.setItemAnimator(new DefaultItemAnimator());
        holder.homeList.setAdapter(homeItemRecyclerAdapter);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

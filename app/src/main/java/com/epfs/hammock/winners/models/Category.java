package com.epfs.hammock.winners.models;

import android.support.annotation.NonNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 6/15/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category implements Comparable {
    private String categoryName;
    private String categoryImageUrl;
    private String categoryId;
    private int counter;

    @Override
    public int compareTo(@NonNull Object o) {
        int compareCount = ((Category) o).counter;
        return compareCount-this.counter;
    }
}

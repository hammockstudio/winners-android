package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.CategoriesPreferenceRecyclerAdapter;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Category;

import java.util.ArrayList;
import java.util.List;

public class PreferencesActivity extends AppCompatActivity implements CategoriesPreferenceRecyclerAdapter.RecyclerViewClickListener {
    RecyclerView categories_list;
    CategoriesPreferenceRecyclerAdapter categoriesPreferenceRecyclerAdapter;
    List<Category> categoryList = new ArrayList<>();

    ProgressDialog progressDialog;
    FireBaseHandler fireBaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        progressDialog = new ProgressDialog(this);
        fireBaseHandler = new FireBaseHandler(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.preferences);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        if(getIntent().getExtras().getBoolean("fromReg"))

        categories_list = (RecyclerView) findViewById(R.id.categories_list);
        categoriesPreferenceRecyclerAdapter = new CategoriesPreferenceRecyclerAdapter(categoryList, PreferencesActivity.this, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 4);
        categories_list.setLayoutManager(mLayoutManager);
        categories_list.setItemAnimator(new DefaultItemAnimator());
        categories_list.setAdapter(categoriesPreferenceRecyclerAdapter);
        initList(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_category_preference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_done:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initList(boolean scrollToTop) {
        categoryList = FireBaseHandler.categoryItems;
        categoriesPreferenceRecyclerAdapter.setData(categoryList);
        categoriesPreferenceRecyclerAdapter.notifyDataSetChanged();
        if (scrollToTop)
            ((ScrollView) findViewById(R.id.scrollView)).smoothScrollTo(0, 0);

    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        if (position >= 0) {
            String categoryId = categoryList.get(position).getCategoryId();
            if (Account.winnersPerson.getCategories().contains(categoryId)) {
                fireBaseHandler.switchCategoryPreference(categoryList.get(position).getCategoryId(), Account.winnersUser.getUid(), false);

            } else {
                fireBaseHandler.switchCategoryPreference(categoryList.get(position).getCategoryId(), Account.winnersUser.getUid(), true);

            }
        }
    }
}

package com.epfs.hammock.winners.adapters.UIObjects;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.DealActivity;
import com.epfs.hammock.winners.models.Images;
import com.epfs.hammock.winners.models.SliderItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 6/9/17.
 */

public class PagerImagesAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    //    int[] images;
    List<Images> sliderItems = new ArrayList<>();

    public PagerImagesAdapter(Context context, List<Images> sliderItems) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sliderItems = sliderItems;
    }

    @Override
    public int getCount() {
        return sliderItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.listitem_advertisement, container, false);
        if (!sliderItems.get(position).getImageUrl().isEmpty())

            Picasso.with(mContext).load(sliderItems.get(position).getImageUrl()).into((ImageView) itemView.findViewById(R.id.imgAdvertisement));
        else
            Picasso.with(mContext).load(R.drawable.placeholder).into((ImageView) itemView.findViewById(R.id.imgAdvertisement));


        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

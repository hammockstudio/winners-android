package com.epfs.hammock.winners.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 7/25/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Claim {
    String uid;
    String claimTime;
}

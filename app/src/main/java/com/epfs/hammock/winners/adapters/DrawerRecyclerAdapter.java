package com.epfs.hammock.winners.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.epfs.hammock.winners.activities.HomePageActivity;
import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.models.DrawerListItem;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class DrawerRecyclerAdapter extends RecyclerView.Adapter<DrawerRecyclerAdapter.ViewHolder>  {
    private List<DrawerListItem> items;
    private static DrawerRecyclerAdapter.RecyclerViewClickListener itemListener;


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView txtMainItemName;

        public ViewHolder(View itemView) {
            super(itemView);
            txtMainItemName = (TextView) itemView.findViewById(R.id.txtMenuName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }
    }

    public DrawerRecyclerAdapter(List<DrawerListItem> items, HomePageActivity itemListener) {
        this.items = items;
        this.itemListener = itemListener;
    }

    @Override
    public DrawerRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_drawer_item, parent, false);
        return new DrawerRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DrawerRecyclerAdapter.ViewHolder holder, int position) {
        DrawerListItem item = items.get(position);
        holder.txtMainItemName.setText(item.getName());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }
}

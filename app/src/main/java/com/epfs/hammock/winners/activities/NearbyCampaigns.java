package com.epfs.hammock.winners.activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.NearbyCampaignAdapter;
import com.epfs.hammock.winners.geo.GeoListener;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Campaign;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NearbyCampaigns extends AppCompatActivity implements FireBaseHandler.OnCampaignsRecieved {

    FireBaseHandler fireBaseHandler = new FireBaseHandler();
    List<Campaign> validCampaigns = new ArrayList<>();

    ViewPager pagerNearbyCampaigns;

    GoogleMap googleMapG;

    MapView mapView;

    HashMap<Marker, Campaign> markers = new HashMap<>();

    List<String> campaignIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_campaigns);
        pagerNearbyCampaigns = (ViewPager) findViewById(R.id.pagerNearbyCampaigns);
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        if (getIntent().hasExtra("campaignId"))
            campaignIds = getIntent().getExtras().getStringArrayList("campaignId");

        pagerNearbyCampaigns.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("onPageSelected", "onPageSelected: " + position);
                try {
                    googleMapG.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                            validCampaigns.get(position).getLat(),
                            validCampaigns.get(position).getLng()), 19f), 200, null);
                } catch (Exception ex) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.nearby_deals);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        if (GeoListener.nearbyCampaigns.size() == 0 && campaignIds.size() == 0) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog))
                    .setMessage("No Deals found nearby..")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
        if (GeoListener.nearbyCampaigns.size() > 0) {
            fireBaseHandler.getCampaignByIds(GeoListener.nearbyCampaigns, this);
        } else {
            if (campaignIds != null)
                fireBaseHandler.getCampaignByIds(campaignIds, this);
        }
    }

    private int getPageByIndex(String campaignId) {
        for (int i = 0; i < validCampaigns.size(); i++) {
            if (validCampaigns.get(i).getCampaignId().equals(campaignId))
                return i;
        }
        return 0;
    }

    @Override
    public void onCampaignsRecieved(final List<Campaign> campaigns) {
        if (campaigns.size() > 0) {
            validCampaigns = campaigns;
            NearbyCampaignAdapter pagerAdapter = new NearbyCampaignAdapter(this, campaigns);
            pagerNearbyCampaigns.setAdapter(pagerAdapter);

            TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_dots);
            tabLayout.setupWithViewPager(pagerNearbyCampaigns, true);

            mapView.onResume();
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setMyLocationEnabled(true);
                    googleMap.setPadding(30, 90, 30, 30);
                    googleMapG = googleMap;
                    LatLng pos;

                    for (Campaign campaign : campaigns) {
                        pos = new LatLng(campaign.getLat(), campaign.getLng());
                        Marker marker;
                        marker = googleMap.addMarker(new MarkerOptions().position(pos)
                                .title(campaign.getCampaignName()));

                        markers.put(marker, campaign);
                    }
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                            validCampaigns.get(0).getLat(),
                            validCampaigns.get(0).getLng()), 19f), 200, null);

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            pagerNearbyCampaigns.setCurrentItem(getPageByIndex(markers.get(marker).getCampaignId()));
                            return true;
                        }
                    });
                }
            });
        }
    }
}

package com.epfs.hammock.winners.models;

import android.graphics.Bitmap;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by allan on 6/5/17.
 */
@Data
@AllArgsConstructor
public class CategoriesItem {
    private Bitmap logo;
    private String name;
}

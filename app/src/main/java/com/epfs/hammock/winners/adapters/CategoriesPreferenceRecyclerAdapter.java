package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */


public class CategoriesPreferenceRecyclerAdapter extends RecyclerView.Adapter<CategoriesPreferenceRecyclerAdapter.ViewHolder> {
    private List<Category> items;
    private Context mContext;
    private FireBaseHandler fireBaseHandler;

    private RecyclerViewClickListener itemListener;



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView image;
        public TextView txtCategoryName;
        public RelativeLayout cat_bg;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtCategoryName);
            cat_bg = (RelativeLayout) itemView.findViewById(R.id.cat_bg);
            itemView.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View view) {
//            Category category = items.get(this.getAdapterPosition());
////            firebaseHandler. items.get(this.getAdapterPosition());
//            if (Account.winnersPerson.getCategories().contains(category.getCategoryId())) {
//            } else {
//                fireBaseHandler.addCategoryPreference(category.getCategoryId(), Account.winnersUser.getUid());
//            }
//
//        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }
    }

    public void setData(List<Category> items) {
        this.items = items;
    }

    public CategoriesPreferenceRecyclerAdapter(List<Category> items, Context context, RecyclerViewClickListener itemListener) {
        this.items = items;
        this.mContext = context;
        this.fireBaseHandler = new FireBaseHandler(context);
        this.itemListener = itemListener;

    }

    @Override
    public CategoriesPreferenceRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listitem_categories_item, parent, false);
        return new CategoriesPreferenceRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoriesPreferenceRecyclerAdapter.ViewHolder holder, int position) {
        Category category = items.get(position);
        holder.txtCategoryName.setText(category.getCategoryName());
        try{
            Picasso.with(mContext).load(items.get(position).getCategoryImageUrl()).into(holder.image);
        }catch(Exception ex){}

        if(Account.winnersPerson.getCategories()!=null){
            if (Account.winnersPerson.getCategories().contains(category.getCategoryId())) {
//                holder.tickSelected.setVisibility(View.VISIBLE);
                holder.cat_bg.setBackground(mContext.getResources().getDrawable(R.drawable.bg_category_selected));
            } else {
//                holder.tickSelected.setVisibility(View.GONE);
                holder.cat_bg.setBackground(mContext.getResources().getDrawable(R.drawable.bg_category));
            }
        }

    }

    @Override
    public int getItemCount() {
                        return items.size();
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }


}

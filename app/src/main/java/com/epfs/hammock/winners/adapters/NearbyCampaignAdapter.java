package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.DealActivity;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.models.Campaign;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 6/9/17.
 */

public class NearbyCampaignAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
//    int[] images;
    List<Campaign> campaigns = new ArrayList<>();

    public NearbyCampaignAdapter(Context context, List<Campaign> campaigns) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.campaigns = campaigns;
    }

    @Override
    public int getCount() {
        return campaigns.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Campaign item = campaigns.get(position);
        View itemView = mLayoutInflater.inflate(R.layout.listitem_deal_smallview_pager, container, false);
        ((TextView)itemView.findViewById(R.id.txtTitle)).setText(item.getCampaignName());
        ((TextView)itemView.findViewById(R.id.txtLongDesc)).setText(Html.fromHtml(item.getDetails()));
        String discount = item.getHighestDiscount().toString();
        if(!discount.equals("0"))
            ((TextView)itemView.findViewById(R.id.txtDiscount)).setText("Up to "+discount + "% off");
        else
            ((TextView)itemView.findViewById(R.id.txtDiscount)).setText("Special Offer");


        if (item.getImages()!=null)
            Picasso.with(mContext).load(item.getImages().get(0).getImageUrl()).into(((ImageView)itemView.findViewById(R.id.imgPromoIcon)));
        else
            ((ImageView)itemView.findViewById(R.id.imgPromoIcon)).setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder));
        if (Account.winnersPerson.getCampaignsFavourites().contains(item.getCampaignId())) {
            (itemView.findViewById(R.id.indicatorFavourite)).setVisibility(View.VISIBLE);
        } else {
            (itemView.findViewById(R.id.indicatorFavourite)).setVisibility(View.GONE);
        }
        container.addView(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DealActivity.class);
                intent.putExtra("campaignId", campaigns.get(position).getCampaignId());
                mContext.startActivity(intent);
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

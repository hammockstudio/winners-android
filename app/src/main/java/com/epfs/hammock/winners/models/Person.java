package com.epfs.hammock.winners.models;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 6/28/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    String personId;
    String personName;
    String personEmail;
    String personImageUrl;
    String personUserRole;
    String membershipLevel;
    List<String> campaignsClaimed = new ArrayList<>();
    List<String> categories = new ArrayList<>();
    List<String> campaignsFavourites = new ArrayList<>();
    boolean recieveNotification;
    boolean sendGeo;

}

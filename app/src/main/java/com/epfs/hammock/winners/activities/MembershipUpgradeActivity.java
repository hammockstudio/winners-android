package com.epfs.hammock.winners.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.WebXHandler;
import com.epfs.hammock.winners.helpers.Commons;

import java.util.HashMap;

public class MembershipUpgradeActivity extends AppCompatActivity {

    WebXHandler webXHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memebership_upgrade);

        webXHandler = new WebXHandler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.membership_upgrade);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



//        findViewById(R.id.btnSilverUpgrade).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                webXHandler.sendPost(MembershipUpgradeActivity.this, "20000");
//            }
//        });

//        findViewById(R.id.btnGoldUpgrade).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HashMap<String,String> params = new HashMap<String, String>();
//                params.put("first_name", "steve");
//                params.put("last_name", "peiries");
//                params.put("email", "steve.peiries@gmail.com");
//                params.put("contact_number", "0775135986");
//                params.put("address_line_one", "someValue");
//                params.put("city", "someValue");
//                params.put("state", "someValue");
//                params.put("postal_code", "10280");
//                params.put("country", "someValue");
//                webXHandler.sendPost(MembershipUpgradeActivity.this, "30000", params);
//            }
//        });

        setMembershipLevel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setMembershipLevel() {
        int membershipLevel = Integer.parseInt(Account.winnersPerson.getMembershipLevel());
        switch (membershipLevel) {
            case 0:
                findViewById(R.id.btnBronzeUpgrade).setVisibility(View.GONE);
                break;
            case 1:
                findViewById(R.id.btnBronzeUpgrade).setVisibility(View.GONE);
                findViewById(R.id.btnSilverUpgrade).setVisibility(View.GONE);
                break;
            case 2:
                findViewById(R.id.btnBronzeUpgrade).setVisibility(View.GONE);
                findViewById(R.id.btnSilverUpgrade).setVisibility(View.GONE);
                findViewById(R.id.btnGoldUpgrade).setVisibility(View.GONE);
                break;
        }
    }

}

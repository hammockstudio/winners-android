package com.epfs.hammock.winners.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.geo.GeoListener;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.geo.Geo;
import com.epfs.hammock.winners.models.Campaign;
import com.epfs.hammock.winners.models.Person;

import java.util.List;

public class SettingsActivity extends AppCompatActivity implements FireBaseHandler.SettingsUpdate {

    FireBaseHandler fireBaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        fireBaseHandler = new FireBaseHandler(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupView();

//        findViewById(R.id.btnUpgrade).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(SettingsActivity.this, MembershipUpgradeActivity.class));
//            }
//        });
    }


    private void saveGeoSettings(boolean b) {
        fireBaseHandler.saveSettings(Account.winnersUser.getUid(), Account.winnersPerson.isRecieveNotification(), b, this);
        if (b) {
            fireBaseHandler.getCampaigns(this, Account.winnersPerson.getCategories(), null, Account.winnersPerson.getPersonId(), 100, new FireBaseHandler.OnCampaignsRecieved() {
                @Override
                public void onCampaignsRecieved(List<Campaign> campaigns) {
                    GeoListener.enableListener(new Geo(getApplicationContext(), campaigns));
                }
            });
        } else
            GeoListener.disableListener();

    }

    private void saveNotificationSettings(boolean b) {
        fireBaseHandler.saveSettings(Account.winnersUser.getUid(), b, Account.winnersPerson.isSendGeo(), this);
    }

    private void setMembershipLevel() {
        int membershipLevel = Integer.parseInt(Account.winnersPerson.getMembershipLevel());
        ((TextView)findViewById(R.id.txtMedalLevel)).setText(Commons.getMemberShip(membershipLevel).toUpperCase());
        switch (membershipLevel) {
            case 0:
                ((ImageView) findViewById(R.id.imgMedal)).setImageDrawable(getResources().getDrawable(R.drawable.medal_bronze));
                ((ImageView) findViewById(R.id.imgMedalHigh)).setImageDrawable(getResources().getDrawable(R.drawable.medal_silver));
                findViewById(R.id.imgMedalLow).setVisibility(View.GONE);

                break;
            case 1:
                ((ImageView) findViewById(R.id.imgMedal)).setImageDrawable(getResources().getDrawable(R.drawable.medal_silver));
                ((ImageView) findViewById(R.id.imgMedalLow)).setImageDrawable(getResources().getDrawable(R.drawable.medal_bronze));
                ((ImageView) findViewById(R.id.imgMedalHigh)).setImageDrawable(getResources().getDrawable(R.drawable.medal_gold));
                break;
            case 2:
                ((ImageView) findViewById(R.id.imgMedal)).setImageDrawable(getResources().getDrawable(R.drawable.medal_gold));
                ((ImageView) findViewById(R.id.imgMedalLow)).setImageDrawable(getResources().getDrawable(R.drawable.medal_silver));
                findViewById(R.id.txtHighestLevelText).setVisibility(View.VISIBLE);
                findViewById(R.id.txtInst).setVisibility(View.GONE);
                findViewById(R.id.imgMedalHigh).setVisibility(View.GONE);
//                findViewById(R.id.btnUpgrade).setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSettingsUpdate(Person person) {
        Account.winnersPerson = person;
        setupView();
    }

    void setupView() {
        Person person = Account.winnersPerson;
        ((Switch) findViewById(R.id.switchGeo)).setChecked(person.isSendGeo());
        ((Switch) findViewById(R.id.switchNotifications)).setChecked(person.isRecieveNotification());
        setupListener();
        setMembershipLevel();

    }

    private void setupListener() {
        (findViewById(R.id.switchGeo)).setEnabled(true);
        ((Switch) findViewById(R.id.switchGeo)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                compoundButton.setOnCheckedChangeListener(null);
                compoundButton.setEnabled(false);
                saveGeoSettings(b);
            }
        });

        (findViewById(R.id.switchNotifications)).setEnabled(true);
        ((Switch) findViewById(R.id.switchNotifications)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                compoundButton.setOnCheckedChangeListener(null);
                compoundButton.setEnabled(false);
                saveNotificationSettings(b);
            }
        });
    }
}

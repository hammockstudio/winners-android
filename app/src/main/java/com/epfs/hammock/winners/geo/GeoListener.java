package com.epfs.hammock.winners.geo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.WelcomeActivity;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Campaign;

import java.util.ArrayList;
import java.util.List;

import static android.media.RingtoneManager.getDefaultUri;

/**
 * Created by allan on 7/12/17.
 */

public class GeoListener{
    public static List<String> nearbyCampaigns = new ArrayList<>();
    public static Geo geo;
    static OnGeoRecieved onGeoRecieved;

    public static void disableListener() {
        if (geo != null)
            geo.disconnectGeoFence();
        geo = null;
    }

    public static void enableListener(Geo mGeo) {
        if (geo != null)
            geo.disconnectGeoFence();
        geo = null;
        geo = mGeo;
    }

    public static boolean isEnabled() {
        return (geo!=null);
    }

    public static void setGeoNearby(final List<String> nearbyCampaigns){
        GeoListener.nearbyCampaigns = nearbyCampaigns;
        FireBaseHandler fireBaseHandler = new FireBaseHandler();
        fireBaseHandler.getCampaignByIds(nearbyCampaigns, new FireBaseHandler.OnCampaignsRecieved() {
            @Override
            public void onCampaignsRecieved(List<Campaign> campaigns) {
                if(onGeoRecieved!=null){
                    onGeoRecieved.onGeoRecieved(campaigns);
                }
            }
        });

    }

    public static void setGeoNearbyListener(OnGeoRecieved onGeoRecieved){
        GeoListener.onGeoRecieved = onGeoRecieved;
    }

    public interface OnGeoRecieved{
         void onGeoRecieved(List<Campaign> nearbyCampaigns);
    }

    public static void setNotification(Context context, ArrayList<String> campaignIds) {
        if(campaignIds.size()>0) {
            int requestID = (int) System.currentTimeMillis();
            Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = new Intent(context, WelcomeActivity.class);
            notificationIntent.putStringArrayListExtra("notificationCampaignId", campaignIds);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent contentIntent = PendingIntent.getActivity(context, requestID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.winnerlogoonly)
                    .setContentTitle("Deals nearby!")
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText("There are deals nearby! Press to check them out!"))
                    .setContentText("There are deals nearby! Press to check them out!").setAutoCancel(true);
            mBuilder.setSound(alarmSound);
            mBuilder.setOnlyAlertOnce(true);
            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(9, mBuilder.build());
        }
    }
}

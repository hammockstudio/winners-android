package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.activities.BecomePartner;
import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.models.Vendor;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class MerchantRecyclerAdapter extends RecyclerView.Adapter<MerchantRecyclerAdapter.ViewHolder> {
    private List<Vendor> items;
    private static RecyclerViewClickListener itemListener;
    private Context mContext;
    private static final int HEADER_VIEW = 1;


    public MerchantRecyclerAdapter(List<Vendor> items, RecyclerViewClickListener itemListener, Context context) {
        this.items = items;
        this.itemListener = itemListener;
        this.mContext = context;
    }

    public void setData(List<Vendor> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_VIEW) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_merchants_item_join, parent, false);
            return new MerchantRecyclerAdapter.FooterViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_merchants_item, parent, false);
            return new ViewHolder(v);
        }
    }

    public class FooterViewHolder extends MerchantRecyclerAdapter.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, BecomePartner.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            if (holder instanceof MerchantRecyclerAdapter.FooterViewHolder) {
                MerchantRecyclerAdapter.FooterViewHolder vh = (MerchantRecyclerAdapter.FooterViewHolder) holder;
            } else if (holder instanceof MerchantRecyclerAdapter.ViewHolder) {
                Vendor item = items.get(position);
                holder.txtTitle.setText(item.getVendorName());
                if (item.getVendorImageUrl() != null)
                    Picasso.with(mContext).load(item.getVendorImageUrl()).into(holder.imgPromotion);
                else
                    holder.imgPromotion.setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder));
            }

        } catch (Exception ex) {
            Log.d("Exception onBind", ex.toString());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == items.size() - 1) {
            return HEADER_VIEW;
        }

        return super.getItemViewType(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgPromotion;
        public TextView txtTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imgPromotion = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtTitle = (TextView) itemView.findViewById(R.id.txtCategoryName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }


}

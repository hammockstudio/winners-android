package com.epfs.hammock.winners.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 7/19/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Images {
    String imageRef;
    String imageUrl;
}

package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.MerchantRecyclerAdapter;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Campaign;
import com.epfs.hammock.winners.models.Vendor;

import java.util.ArrayList;
import java.util.List;

public class MerchantActivity extends RootActivity implements MerchantRecyclerAdapter.RecyclerViewClickListener,
        FireBaseHandler.OnCampaignsRecieved, FireBaseHandler.OnVendorsRecieved {
    RecyclerView categoryList;
    List<Vendor> merchantItems = new ArrayList<>();
    MerchantRecyclerAdapter merchantRecyclerAdapter;
    FireBaseHandler fireBaseHandler;

    ProgressDialog progressDialog;

    List<String> categoryIds = new ArrayList<>();
    List<String> merchantIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant);
        fireBaseHandler = new FireBaseHandler(this);
        progressDialog = new ProgressDialog(this);
        (findViewById(R.id.txtNullMessage)).setVisibility(View.GONE);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.category);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getStringExtra("categoryName") != null) {
            categoryIds.add(getIntent().getStringExtra("category"));
            getSupportActionBar().setTitle(getIntent().getStringExtra("categoryName"));
        }

        if (getIntent().getStringExtra("merchantId") != null) {
            merchantIds.add(getIntent().getStringExtra("merchantId"));
        }

        categoryList = (RecyclerView) findViewById(R.id.merchantList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        categoryList.setLayoutManager(mLayoutManager);
        categoryList.setItemAnimator(new DefaultItemAnimator());

        merchantRecyclerAdapter = new MerchantRecyclerAdapter(merchantItems, this, this.getApplicationContext());
        merchantRecyclerAdapter.notifyDataSetChanged();
        categoryList.setAdapter(merchantRecyclerAdapter);

        progressDialog.setMessage("Loading.. ");
        progressDialog.show();
        fireBaseHandler.getCampaigns(this, categoryIds, merchantIds, null, 100, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(MerchantActivity.this, CategoryActivity.class);
        intent.putExtra("merchantName", merchantItems.get(position).getVendorName());
        intent.putExtra("merchantId", merchantItems.get(position).getVendorId());
        intent.putExtra("category", getIntent().getStringExtra("category"));
        startActivity(intent);
    }

    @Override
    public void onCampaignsRecieved(List<Campaign> campaigns) {
        List<String> vendorIds = new ArrayList<>();
        for (Campaign campaign : campaigns) {
            if (!vendorIds.contains(campaign.getVendorId()))
                vendorIds.add(campaign.getVendorId());
        }
        fireBaseHandler.getMerchantByIds(vendorIds, this);
//        fireBaseHandler.getMerchants(this);

    }

    @Override
    public void onVendorsRecieved(List<Vendor> vendors) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        merchantItems = vendors;
        merchantItems.add(new Vendor("Become a preferred partner", "", ""));
        merchantRecyclerAdapter.setData(vendors);
        merchantRecyclerAdapter.notifyDataSetChanged();

        if (vendors.size() <= 1) {
            (findViewById(R.id.txtNullMessage)).setVisibility(View.VISIBLE);
        } else {
            (findViewById(R.id.txtNullMessage)).setVisibility(View.GONE);
        }
    }
}

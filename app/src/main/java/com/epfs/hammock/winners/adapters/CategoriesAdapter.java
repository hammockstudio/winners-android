package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.MerchantActivity;
import com.epfs.hammock.winners.activities.PreferencesActivity;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    private List<Category> items;
    private Context mContext;
    private FireBaseHandler fireBaseHandler;

    private static final int FOOTER_VIEW = 1;

    private boolean displayFooter = false;

    public class FooterViewHolder extends ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, PreferencesActivity.class));
                }
            });
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView image;
        public TextView txtCategoryName;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtCategoryName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, MerchantActivity.class);
            intent.putExtra("category", items.get(this.getAdapterPosition()).getCategoryId());
            intent.putExtra("categoryName", items.get(this.getAdapterPosition()).getCategoryName());
            mContext.startActivity(intent);
        }
    }

    public void setData(List<Category> items) {
        this.items = items;
    }

    public CategoriesAdapter(List<Category> items, Context context, boolean displayFooter) {
        this.items = items;
        this.mContext = context;
        this.fireBaseHandler = new FireBaseHandler(context);
        this.displayFooter = displayFooter;

    }

    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listitem_categories_item_white, parent, false);

        if (viewType == FOOTER_VIEW) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_categories_item_addmore, parent, false);

            FooterViewHolder vh = new FooterViewHolder(v);

            return vh;
        }

        return new CategoriesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoriesAdapter.ViewHolder holder, int position) {

        try {
            if (holder instanceof FooterViewHolder) {
                FooterViewHolder vh = (FooterViewHolder) holder;
            } else if (holder instanceof ViewHolder) {
                Category category = items.get(position);
                holder.txtCategoryName.setText(category.getCategoryName());
                Picasso.with(mContext).load(items.get(position).getCategoryImageUrl()).into(holder.image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (displayFooter) {
            if (position == items.size() - 1) {
                // This is where we'll add footer.
                return FOOTER_VIEW;
            }
        }

        return super.getItemViewType(position);
    }


}

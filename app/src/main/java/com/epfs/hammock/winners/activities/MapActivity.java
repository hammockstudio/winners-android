package com.epfs.hammock.winners.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.NearbyCampaignAdapter;
import com.epfs.hammock.winners.adapters.UIObjects.CustomInfoWindowAdapter;
import com.epfs.hammock.winners.geo.GeoListener;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Campaign;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity implements FireBaseHandler.OnCampaignsRecieved, LocationListener {
    MapView mapView;

    FireBaseHandler fireBaseHandler;

    GoogleMap googleMap = null;

    Context mContext;

    ProgressDialog progressDialog;

    ClusterManager<ClusterMarkerLocation> clusterManager;
    ClusterMarkerLocation selectedClusterMarker;

    LocationManager locationManager;

    List<String> campaignIds = new ArrayList<>();
    List<Campaign> validCampaigns = new ArrayList<>();

    boolean isAskedPermission = false;
    boolean useGps = false;
    boolean isNearbyVisible = true;

    ViewPager pagerNearbyCampaigns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fireBaseHandler = new FireBaseHandler(this);
        progressDialog = new ProgressDialog(this);
        mContext = this;

        setContentView(R.layout.activity_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.deals_nearby);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().hasExtra("campaignId"))
            campaignIds = getIntent().getExtras().getStringArrayList("campaignId");

        pagerNearbyCampaigns = (ViewPager) findViewById(R.id.pagerNearbyCampaigns);
        pagerNearbyCampaigns.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("onPageSelected", "onPageSelected: " + position);
                try {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                            validCampaigns.get(position).getLat(),
                            validCampaigns.get(position).getLng()), 19f), 200, null);
                } catch (Exception ex) {
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        toggleNearbyView(false);

        progressDialog.setMessage("Loading..");
        progressDialog.show();
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            int off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            if (off == 0) {
                if (!isAskedPermission) {
                    isAskedPermission = true;
                    new AlertDialog.Builder(new ContextThemeWrapper(MapActivity.this, R.style.myDialog))
                            .setMessage("Do you want to turn on your location for more accurate results?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(onGPS);
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            } else {
                useGps = true;
                if (googleMap != null)
                    updateToUserLocation();
            }
        } catch (Exception ex) {
            Log.d("exception", ex.toString());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        fireBaseHandler.getCampaigns(this, null, null, null, 100, this);

//        if (GeoListener.nearbyCampaigns.size() == 0 && campaignIds.size() == 0) {
//            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog))
//                    .setMessage("No Deals found nearby..")
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            finish();
//                        }
//                    })
//                    .show();
//        }
        List<String> validCampaignIds = new ArrayList<>();
        if (GeoListener.nearbyCampaigns.size() > 0) {
            validCampaignIds = GeoListener.nearbyCampaigns;
        } else {
            if (campaignIds != null)
                validCampaignIds = campaignIds;
        }

        fireBaseHandler.getCampaignByIds(validCampaignIds, new FireBaseHandler.OnCampaignsRecieved() {
            @Override
            public void onCampaignsRecieved(List<Campaign> campaigns) {
                validCampaigns = campaigns;
                NearbyCampaignAdapter pagerAdapter = new NearbyCampaignAdapter(MapActivity.this, campaigns);
                pagerNearbyCampaigns.setAdapter(pagerAdapter);

                TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_dots);
                tabLayout.setupWithViewPager(pagerNearbyCampaigns, true);
            }
        });

    }

    private void initMarkers(GoogleMap mGoogleMap, final List<Campaign> campaigns) {
        googleMap = mGoogleMap;
        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mGoogleMap.setOnCameraChangeListener(clusterManager);
        for (int i = 0; i < campaigns.size(); i++) {
            if (campaigns.get(i).getLat() != -1 && campaigns.get(i).getLng() != -1) {
                final Campaign campaign = campaigns.get(i);
                clusterManager.addItem(new ClusterMarkerLocation(campaign));
                builder.include(new LatLng(campaign.getLat(), campaign.getLng()));
            }
        }
        progressDialog.dismiss();
        LatLngBounds bounds = builder.build();
        if (!useGps)
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 60));
        else
            updateToUserLocation();
        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter(mContext));
    }

    private void updateToUserLocation() {
        progressDialog.setMessage("Getting your location..");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        progressDialog.show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager = (LocationManager) getSystemService
                (Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        locationManager.requestSingleUpdate(criteria, new android.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    double currentLongitude = location.getLongitude();
                    double currentLatitude = location.getLatitude();
                    LatLng currentLocation = new LatLng(currentLatitude, currentLongitude);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLocation)
                            .zoom(17)
                            .bearing(0)
                            .tilt(0)
                            .build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        }, null);

    }

    private int getPageByIndex(String campaignId) {
        for (int i = 0; i < validCampaigns.size(); i++) {
            if (validCampaigns.get(i).getCampaignId().equals(campaignId))
                return i;
        }
        return -99;
    }

    private void toggleNearbyView(boolean show) {
        if (show) {
            if (!isNearbyVisible) {
                isNearbyVisible = true;

                Animation bottomUp = AnimationUtils.loadAnimation(MapActivity.this,
                        R.anim.bottom_up);

                ViewGroup hiddenPanel = (ViewGroup) findViewById(R.id.lytNearbyCampaigns);
                hiddenPanel.setVisibility(View.VISIBLE);
                hiddenPanel.startAnimation(bottomUp);
                if (googleMap != null)
                    googleMap.setPadding(0, 0, 0, Commons.getPix(MapActivity.this, 340));

            } else {
            }
        } else {
            if (isNearbyVisible) {
                isNearbyVisible = false;

                Animation bottomDown = AnimationUtils.loadAnimation(MapActivity.this,
                        R.anim.bottom_down);
                ViewGroup hiddenPanel = (ViewGroup) findViewById(R.id.lytNearbyCampaigns);
                hiddenPanel.startAnimation(bottomDown);
                hiddenPanel.setVisibility(View.INVISIBLE);
                if (googleMap != null)
                    googleMap.setPadding(0, 0, 0, 0);

            } else {
            }

        }
    }


    @Override
    public void onCampaignsRecieved(final List<Campaign> campaigns) {
        if (googleMap == null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            toggleNearbyView(false);
                        }
                    });

                    googleMap.setMyLocationEnabled(true);
                    googleMap.getUiSettings().setZoomGesturesEnabled(true);
                    googleMap.getUiSettings().setCompassEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);

                    clusterManager = new ClusterManager<ClusterMarkerLocation>(mContext, googleMap);
                    googleMap.setInfoWindowAdapter(clusterManager.getMarkerManager());
                    googleMap.setOnMarkerClickListener(clusterManager);
                    clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterMarkerLocation>() {
                        @Override
                        public boolean onClusterItemClick(ClusterMarkerLocation clusterMarkerLocation) {
                            selectedClusterMarker = clusterMarkerLocation;
                            int index = getPageByIndex(clusterMarkerLocation.getCampaign().getCampaignId());
                            if (index != -99) {
                                pagerNearbyCampaigns.setCurrentItem(index);
//                                return true;
                                googleMap.setInfoWindowAdapter(null);
                                toggleNearbyView(true);

                            } else {
                                googleMap.setInfoWindowAdapter(clusterManager.getMarkerManager());
                                toggleNearbyView(false);
                            }

                            return false;
                        }
                    });
                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, DealActivity.class);
                            intent.putExtra("campaignId", selectedClusterMarker.getCampaign().getCampaignId());
                            startActivity(intent);
                        }
                    });

                    initMarkers(googleMap, campaigns);
                }
            });
        } else
            initMarkers(googleMap, campaigns);
    }


    public ClusterMarkerLocation getMarkerCampaign() {
        return selectedClusterMarker;
    }

    @Override
    public void onLocationChanged(Location location) {

    }


    public class ClusterMarkerLocation implements ClusterItem {

        private LatLng position;
        private Campaign campaign;

        public ClusterMarkerLocation(Campaign campaign) {
            this.position = new LatLng(campaign.getLat(), campaign.getLng());
            this.campaign = campaign;

        }

        @Override
        public LatLng getPosition() {
            return position;
        }

        public void setPosition(LatLng position) {
            this.position = position;
        }

        public Campaign getCampaign() {
            return campaign;
        }
    }


}

package com.epfs.hammock.winners.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 7/25/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SliderItem {
    String campaignId;
    String campaignName;
    String sliderImageUrl;
    String sliderImageRef;
    String sliderLink;
}

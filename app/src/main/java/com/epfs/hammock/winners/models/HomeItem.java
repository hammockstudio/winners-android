package com.epfs.hammock.winners.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by allan on 6/5/17.
 */
@Data
@AllArgsConstructor
public class HomeItem {

    private String name;
    private List<Campaign> homeListItems;

}

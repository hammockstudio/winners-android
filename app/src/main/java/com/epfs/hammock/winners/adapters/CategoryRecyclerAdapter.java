package com.epfs.hammock.winners.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Campaign;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.ViewHolder> {
    private List<Campaign> items;
    private static RecyclerViewClickListener itemListener;
    private Context mContext;

    public CategoryRecyclerAdapter(List<Campaign> items, RecyclerViewClickListener itemListener, Context context) {
        this.items = items;
        this.itemListener = itemListener;
        this.mContext = context;
    }

    public void setData(List<Campaign> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_deal_bigview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Campaign item = items.get(position);
        try {
            holder.txtTitle.setText(item.getCampaignName());
            if (item.getCampaignName().length() > 20)
                holder.txtTitle.setTextSize(19);
            holder.txtLongDesc.setText(Html.fromHtml(item.getDetails()).toString().trim());

            String discount = item.getHighestDiscount().toString();
            if(!discount.equals("0"))
                holder.txtDiscount.setText("Up to "+discount + "% off");
            else
                holder.txtDiscount.setText("Special Offer");


//            Picasso.with(mContext).load(item.getTileImageUrl()).into(holder.imgPromotion);

            if (item.getImages() != null && item.getImages().get(0).getImageUrl() != null)
                Picasso.with(mContext).load(item.getImages().get(0).getImageUrl()).into(holder.imgPromotion, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
            else {
                holder.imgPromotion.setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder));
//                holder.itemView.findViewById(R.id.lytTitle).setBackground(null);
                holder.itemView.findViewById(R.id.lytDiscount).setBackground(null);
                holder.progressBar.setVisibility(View.GONE);
            }

            getHeight(holder);
        } catch (Exception ex) {
            Log.d("Exception onBind", ex.toString());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgPromotion;
        public TextView txtTitle;
        public TextView txtLongDesc;
        public TextView txtDiscount;
        public RelativeLayout lytPromotionImage;
        public ProgressBar progressBar;
        public View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imgPromotion = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtLongDesc = (TextView) itemView.findViewById(R.id.txtLongDesc);
            txtDiscount = (TextView) itemView.findViewById(R.id.txtDiscount);
            lytPromotionImage = (RelativeLayout) itemView.findViewById(R.id.lytPromotionImg);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }

    private void getHeight(ViewHolder holder) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels - Commons.getPix(mContext, 40);


        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.lytPromotionImage.findViewById(R.id.lytPromotionImg).getLayoutParams();
        params.height = (int) (((float) 9 / 16) * width);
        holder.lytPromotionImage.findViewById(R.id.lytPromotionImg).setLayoutParams(params);
    }

}

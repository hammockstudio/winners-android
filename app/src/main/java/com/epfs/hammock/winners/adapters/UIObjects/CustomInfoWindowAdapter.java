package com.epfs.hammock.winners.adapters.UIObjects;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.MapActivity;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Vendor;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 7/4/17.
 */

public class CustomInfoWindowAdapter implements InfoWindowAdapter {

    private View view;
    private Context mContext;

    private LayoutInflater layoutInflater;

    FireBaseHandler fireBaseHandler = new FireBaseHandler();


    public CustomInfoWindowAdapter(Context context) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.common_custom_infoview, null);
        this.mContext = context;
    }

    @Override
    public View getInfoContents(Marker mark) {
        return view;
    }

    @Override
    public View getInfoWindow(final Marker mark) {
        MapActivity.ClusterMarkerLocation clusterMarkerLocation = ((MapActivity) mContext).getMarkerCampaign();
        ((TextView) view.findViewById(R.id.txtTitle)).setText(clusterMarkerLocation.getCampaign().getCampaignName());
        ((TextView) view.findViewById(R.id.txtMerchantName)).setText(Html.fromHtml(clusterMarkerLocation.getCampaign().getVendorName()));
//        ((TextView) view.findViewById(R.id.txtDiscount)).setText(clusterMarkerLocation.getCampaign().getDiscount() + "% off");

        fireBaseHandler.getMerchantById(clusterMarkerLocation.getCampaign().getVendorId(), new FireBaseHandler.OnVendorsRecieved() {
            @Override
            public void onVendorsRecieved(final List<Vendor> vendors) {
                if (vendors.get(0).getVendorImageUrl() != null)
                Picasso.with(mContext).load(
                        vendors.get(0).getVendorImageUrl()).into(((ImageView) view.findViewById(R.id.imgPromoIcon)),
                        new MarkerCallback(mark));
            }
        });


        return view;
    }

    static class MarkerCallback implements Callback {
        Marker marker = null;

        MarkerCallback(Marker marker) {
            this.marker = marker;
        }

        @Override
        public void onError() {
            Log.e(getClass().getSimpleName(), "Error loading thumbnail!");
        }

        @Override
        public void onSuccess() {
            if (marker != null && marker.isInfoWindowShown()) {
                marker.showInfoWindow();
            }
        }
    }
}
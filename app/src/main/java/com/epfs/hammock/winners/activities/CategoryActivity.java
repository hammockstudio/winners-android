package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.CategoryRecyclerAdapter;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Campaign;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends RootActivity implements CategoryRecyclerAdapter.RecyclerViewClickListener,
        FireBaseHandler.OnCampaignsRecieved {
    RecyclerView categoryList;
    List<Campaign> campaignItems = new ArrayList<>();
    CategoryRecyclerAdapter categoryRecyclerAdapter;
    FireBaseHandler fireBaseHandler;

    ProgressDialog progressDialog;

    List<String> categoryIds = new ArrayList<>();
    List<String> merchantIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        fireBaseHandler = new FireBaseHandler(this);
        progressDialog = new ProgressDialog(this);
        (findViewById(R.id.txtNullMessage)).setVisibility(View.GONE);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getStringExtra("merchantName") != null) {
            merchantIds.add(getIntent().getStringExtra("merchantId"));
            getSupportActionBar().setTitle(getIntent().getStringExtra("merchantName"));
        }

        if (getIntent().getStringExtra("category") != null) {
            categoryIds.add(getIntent().getStringExtra("category"));
        }

        categoryList = (RecyclerView) findViewById(R.id.categoryList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        categoryList.setLayoutManager(mLayoutManager);
        categoryList.setItemAnimator(new DefaultItemAnimator());

        categoryRecyclerAdapter = new CategoryRecyclerAdapter(campaignItems, this, this.getApplicationContext());
        categoryRecyclerAdapter.notifyDataSetChanged();
        categoryList.setAdapter(categoryRecyclerAdapter);

        progressDialog.setMessage("Loading.. ");
        progressDialog.show();
        fireBaseHandler.getCampaigns(this, categoryIds, merchantIds,null,100, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(CategoryActivity.this, DealActivity.class);
        intent.putExtra("campaignId", campaignItems.get(position).getCampaignId());
        startActivity(intent);
    }

    @Override
    public void onCampaignsRecieved(List<Campaign> campaigns) {
        progressDialog.dismiss();
        campaignItems = campaigns;
        categoryRecyclerAdapter.setData(campaignItems);
        categoryRecyclerAdapter.notifyDataSetChanged();

        if(campaigns.size()<1){
            (findViewById(R.id.txtNullMessage)).setVisibility(View.VISIBLE);
        }else{
            (findViewById(R.id.txtNullMessage)).setVisibility(View.GONE);

        }
    }
}

package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.MerchantActivity;
import com.epfs.hammock.winners.activities.PreferencesActivity;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */


public class CharitiesAdapter extends RecyclerView.Adapter<CharitiesAdapter.ViewHolder> {
    private int[] items;
    private Context mContext;



    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgCharity);
        }

    }

    public void setData(int[] items) {
        this.items = items;
    }

    public CharitiesAdapter(int[] items, Context context) {
        this.items = items;
        this.mContext = context;

    }

    @Override
    public CharitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listitem_charities_item, parent, false);

        return new CharitiesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CharitiesAdapter.ViewHolder holder, int position) {
//        holder.image.setImageDrawable(mContext.getResources().getDrawable(items[position]));
        Picasso.with(mContext).load(items[position]).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


}

package com.epfs.hammock.winners.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.CategoriesAdapter;
import com.epfs.hammock.winners.adapters.DrawerRecyclerAdapter;
import com.epfs.hammock.winners.adapters.HomeRecyclerAdapter;
import com.epfs.hammock.winners.adapters.PagerAdvertisementAdapter;
import com.epfs.hammock.winners.adapters.UIObjects.CustomToolbarHandler;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.geo.GeoListener;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.CircleTransform;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.geo.Geo;
import com.epfs.hammock.winners.models.Campaign;
import com.epfs.hammock.winners.models.Category;
import com.epfs.hammock.winners.models.DrawerListItem;
import com.epfs.hammock.winners.models.HomeItem;
import com.epfs.hammock.winners.models.Person;
import com.epfs.hammock.winners.models.SliderItem;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.epfs.hammock.winners.constants.Account.winnersUser;

public class HomePageActivity extends RootActivity implements DrawerRecyclerAdapter.RecyclerViewClickListener,
        FireBaseHandler.HomeScreenUpdate {
    String TAG = "HomePageActivity";
    static final Integer LOCATION = 0x1;

    DrawerLayout drawer;
    boolean drawerOpen = false;
    boolean isTimerRunning = false;
    boolean initialScrollDone = false;
    boolean isListenerSet = false;
    Timer autoScroller;
    int page = 0;
    int seconds = 3;
    int iteration = 0;

    FireBaseHandler fireBaseHandler;
    CustomToolbarHandler customToolbarHandler;
    Dialog progressDialog;
    List<Category> allCategoriesItems = new ArrayList<>();
    List<Category> categoriesItems = new ArrayList<>();
    List<HomeItem> homeItems = new ArrayList<>();
    List<SliderItem> sliderItems = new ArrayList<>();

    HomeRecyclerAdapter homeRecyclerAdapter;
    CategoriesAdapter allCategoriesAdapter;
    CategoriesAdapter categoriesAdapter;

    ViewPager advertisementPager;
    RecyclerView all_categories_list;
    RecyclerView categories_list;
    RecyclerView homepage_list;
    RecyclerView drawer_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        toggleHome(true);
        fireBaseHandler = new FireBaseHandler(this);

//        fireBaseHandler.updateCounts();

        fireBaseHandler.getAdvertisements(this);
        customToolbarHandler = new CustomToolbarHandler(this, (LinearLayout) findViewById(R.id.lytCustomToolbar), true);
        setAppLive(false);
        setUpView();
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION);
        advertisementPager = (ViewPager) findViewById(R.id.bgPager);

        advertisementPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                resetTimer();
                page = position;
                iteration = 0;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        (findViewById(R.id.scrollView)).setVisibility(View.INVISIBLE);

        findViewById(R.id.btnMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomePageActivity.this, MapActivity.class));
            }
        });
        findViewById(R.id.btnCategories).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleHome(false);
            }
        });
        findViewById(R.id.btnHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleHome(true);
            }
        });
        findViewById(R.id.btnDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleDrawer();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        toggleTimer(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        toggleTimer(true);
        fireBaseHandler.getLatestAppVersion(this);
        fireBaseHandler.getCampaignByIds(GeoListener.nearbyCampaigns, new FireBaseHandler.OnCampaignsRecieved() {
            @Override
            public void onCampaignsRecieved(List<Campaign> campaigns) {
                customToolbarHandler.setNearbyCount(campaigns.size());
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void toggleTimer(boolean on) {
        isTimerRunning = on;
        if (on) {
            autoScroller = new Timer();
            autoScroller.scheduleAtFixedRate(new HomePageActivity.AutoScroller(), 0, seconds * 1000);
        } else {
            autoScroller.cancel();
            iteration = 0;
        }
    }

    private void resetTimer() {
        toggleTimer(false);
        toggleTimer(true);
    }

    private void setUpView() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_list = (RecyclerView) findViewById(R.id.drawer_list);
        setupDrawerMenu(0);

        homepage_list = (RecyclerView) findViewById(R.id.homepage_list);
        homeRecyclerAdapter = new HomeRecyclerAdapter(homeItems, this);
        RecyclerView.LayoutManager hLayoutManager = new LinearLayoutManager(this);
        homepage_list.setLayoutManager(hLayoutManager);
        homepage_list.setItemAnimator(new DefaultItemAnimator());
        homepage_list.setAdapter(homeRecyclerAdapter);

        categories_list = (RecyclerView) findViewById(R.id.categories_list);
        categoriesAdapter = new CategoriesAdapter(categoriesItems, this, true);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 4);
        categories_list.setLayoutManager(mLayoutManager);
        categories_list.setItemAnimator(new DefaultItemAnimator());
        categories_list.setAdapter(categoriesAdapter);

        all_categories_list = (RecyclerView) findViewById(R.id.all_categories_list);
        allCategoriesAdapter = new CategoriesAdapter(allCategoriesItems, this, false);
        RecyclerView.LayoutManager aMLayoutManager = new GridLayoutManager(this, 4);
        all_categories_list.setLayoutManager(aMLayoutManager);
        all_categories_list.setItemAnimator(new DefaultItemAnimator());
        all_categories_list.setAdapter(allCategoriesAdapter);

        homepage_list.setNestedScrollingEnabled(false);
        categories_list.setNestedScrollingEnabled(false);
        all_categories_list.setNestedScrollingEnabled(false);
    }

    private void toggleHome(boolean toggle) {
        if (toggle) {
            findViewById(R.id.lytHome).setVisibility(View.VISIBLE);
            findViewById(R.id.btnHomeIndicator).setVisibility(View.VISIBLE);
            findViewById(R.id.lytCategories).setVisibility(View.GONE);
            findViewById(R.id.btnCategoriesIndicator).setVisibility(View.GONE);
            findViewById(R.id.advertisement_lyt).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.lytHome).setVisibility(View.GONE);
            findViewById(R.id.btnHomeIndicator).setVisibility(View.GONE);
            findViewById(R.id.lytCategories).setVisibility(View.VISIBLE);
            findViewById(R.id.btnCategoriesIndicator).setVisibility(View.VISIBLE);
            findViewById(R.id.advertisement_lyt).setVisibility(View.GONE);
        }
    }

    private void toggleDrawer() {
        if (drawerOpen) {
            drawer.closeDrawer(GravityCompat.START);
        } else
            drawer.openDrawer(Gravity.START);
        drawerOpen = !drawerOpen;
    }

    private void toggleDrawer(boolean open) {
        if (!open) {
            drawer.closeDrawer(GravityCompat.START);
        } else
            drawer.openDrawer(Gravity.START);
        drawerOpen = !drawerOpen;
    }


    private void initHomeList() {
        fireBaseHandler.getCampaigns(this, null, null, null, 10, new FireBaseHandler.OnCampaignsRecieved() {
            @Override
            public void onCampaignsRecieved(List<Campaign> campaigns) {
                homeItems.clear();
                homeItems.add(new HomeItem("EDITOR'S PICKS", campaigns));
                homeItems.add(new HomeItem("RECOMMENDED FOR YOU", campaigns));
                homeRecyclerAdapter.setData(homeItems);
                homeRecyclerAdapter.notifyDataSetChanged();
            }
        });


    }

    private void setupDrawerMenu(int wishListSize) {
        List<DrawerListItem> items = new ArrayList<>();
        if (wishListSize > 0)
            items.add(new DrawerListItem("Wish List (" + wishListSize + ")", null));
        else
            items.add(new DrawerListItem("Wish List", null));

        items.add(new DrawerListItem("Category preferences", null));
        items.add(new DrawerListItem("Settings", null));
        items.add(new DrawerListItem("Charities", null));
        items.add(new DrawerListItem("Become a Preferred Partner", null));
        items.add(new DrawerListItem("Contact Us", null));
        items.add(new DrawerListItem("Log Out", null));

        DrawerRecyclerAdapter drawerRecyclerAdapter = new DrawerRecyclerAdapter(items, this);
        drawer_list.setLayoutManager(new LinearLayoutManager(this));
        drawer_list.setItemAnimator(new DefaultItemAnimator());
        drawer_list.setAdapter(drawerRecyclerAdapter);
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        toggleDrawer(false);
        switch (position) {
            case 0:
                startActivity(new Intent(HomePageActivity.this, Favourites.class));
                break;
            case 1:
                startActivity(new Intent(HomePageActivity.this, PreferencesActivity.class));
                break;
            case 2:
                startActivity(new Intent(HomePageActivity.this, SettingsActivity.class));
                break;
            case 3:
                startActivity(new Intent(HomePageActivity.this, CharitiesActivity.class));
                break;
            case 4:
                startActivity(new Intent(HomePageActivity.this, BecomePartner.class));
                break;
            case 5:
                startActivity(new Intent(HomePageActivity.this, ContactActivity.class));
                break;

            case 6:
                //logout
                new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog))
                        .setMessage("Are you sure you want to Log Out?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Account.logUserOut();
                                startActivity(new Intent(HomePageActivity.this, WelcomeActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            default:
        }
    }

    private void checkForNotificationLaunch() {
        if (getIntent().getStringArrayListExtra("notificationCampaignId") != null) {
            Intent intent = new Intent(this, NearbyCampaigns.class);
            intent.putExtra("campaignId", getIntent().getStringArrayListExtra("notificationCampaignId"));
            startActivity(intent);
            getIntent().removeExtra("notificationCampaignId");
        }
    }

    private void setupAccountInfo() {
        Account.winnersUser = FirebaseAuth.getInstance().getCurrentUser();
        if (Account.winnersUser != null) {
            try {
                String username;
                if (Account.winnersUser.getDisplayName() == null || Account.winnersUser.getDisplayName().equals("")) {
                    username = Account.winnersPerson.getPersonName().split("\\s+")[0];
                } else {
                    username = Account.winnersUser.getDisplayName().split("\\s+")[0];
                }
                ((TextView) findViewById(R.id.txtUsername)).setText(username);

                if (winnersUser.getPhotoUrl() != null && !winnersUser.getPhotoUrl().equals(""))
                    Picasso.with(this).load(winnersUser.getPhotoUrl()).transform(new CircleTransform()).into((ImageView)
                            findViewById(R.id.imgProfilePicture));
                else {
                    Picasso.with(this).load(R.drawable.propic_def).transform(new CircleTransform()).into((ImageView)
                            findViewById(R.id.imgProfilePicture));
                }
            } catch (Exception ex) {
                Log.d(TAG, ex.toString());
            }
        }
    }

    private void setAdvertisementHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        @SuppressLint("WrongViewCast") LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) findViewById(R.id.advertisement_lyt).getLayoutParams();
        params.height = ((int) (((float) 9 / 16) * width));
        findViewById(R.id.advertisement_lyt).setLayoutParams(params);
    }

    private void setClaimCount(int count) {
        if (count < 1)
            findViewById(R.id.lytClaimCount).setVisibility(View.INVISIBLE);
        else {
            findViewById(R.id.lytClaimCount).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.txtClaimCount)).setText(String.valueOf(count));
        }
    }

    public void setAppLive(boolean toggle) {
        if (toggle) {
//            initHomeList();
        } else {
            progressDialog = new Dialog(this);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.dialog_intialize_logo);
            progressDialog.show();
        }
    }


    private void loadCategories() {
        categoriesAdapter.setData(getValidCategories());
        categoriesAdapter.notifyDataSetChanged();

        allCategoriesAdapter.setData(FireBaseHandler.categoryItems);
        allCategoriesAdapter.notifyDataSetChanged();
        (findViewById(R.id.scrollView)).setVisibility(View.VISIBLE);
        if (!initialScrollDone) {
            ((ScrollView) findViewById(R.id.scrollView)).fullScroll(ScrollView.FOCUS_UP);
            initialScrollDone = true;
        }
    }

    private List<Category> getValidCategories() {
        List<Category> validCategory = new ArrayList<>();
        for (Category category : FireBaseHandler.categoryItems) {
            if (Account.winnersPerson.getCategories().contains(category.getCategoryId())) {
                validCategory.add(category);
            }
        }
        validCategory.add(new Category("", "", "",0));
        return validCategory;
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(HomePageActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomePageActivity.this, permission)) {
                ActivityCompat.requestPermissions(HomePageActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.CALL_PHONE}, requestCode);
            } else {
                ActivityCompat.requestPermissions(HomePageActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            Log.d(TAG, "Permission has already been given");
        }
    }

    private void openPreferenceIfFirstTime() {
        boolean fromReg = getIntent().getExtras().getBoolean("fromReg");
        if (fromReg) {
            Intent intent = new Intent(this, PreferencesActivity.class);
            intent.putExtra("fromReg", true);
            startActivity(intent);
            getIntent().putExtra("fromReg", false);
        }
    }

    @Override
    public void onAccountUpdate(Person person) {
        Account.winnersPerson = person;
        if (Account.winnersPerson != null) {
            setupAccountInfo();
            if (Account.winnersPerson.getCampaignsClaimed() != null) {
                fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsClaimed(), new FireBaseHandler.OnCampaignsRecieved() {
                    @Override
                    public void onCampaignsRecieved(List<Campaign> campaigns) {
                        setClaimCount(campaigns.size());
                    }
                });
            }
            if (Account.winnersPerson.isSendGeo()) {
                fireBaseHandler.getCampaigns(this, Account.winnersPerson.getCategories(), null, Account.winnersPerson.getPersonId(), 1000, new FireBaseHandler.OnCampaignsRecieved() {
                    @Override
                    public void onCampaignsRecieved(List<Campaign> campaigns) {
                        GeoListener.enableListener(new Geo(getApplicationContext(), campaigns));
                    }
                });
            }
            fireBaseHandler.setCategoriesListener(this);
            fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsFavourites(), new FireBaseHandler.OnCampaignsRecieved() {
                @Override
                public void onCampaignsRecieved(List<Campaign> campaigns) {
                    setupDrawerMenu(campaigns.size());

                }
            });
        }
    }

    @Override
    public void onCategoriesUpdate(List<Category> categories) {
        loadCategories();
        checkForNotificationLaunch();
        openPreferenceIfFirstTime();

        progressDialog.dismiss();

    }

    @Override
    public void onHomeUpdate() {
        setAppLive(true);
    }

    @Override
    public void onSliderUpdate(List<SliderItem> sliderItems) {
        this.sliderItems = sliderItems;

        PagerAdvertisementAdapter pagerAdapter = new PagerAdvertisementAdapter(this, sliderItems);
        advertisementPager.setAdapter(pagerAdapter);
        setAdvertisementHeight();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_dots);
        tabLayout.setupWithViewPager(advertisementPager, true);

        advertisementPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (isTimerRunning)
                        toggleTimer(false);
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (!isTimerRunning)
                        toggleTimer(true);
                }
                return false;
            }
        });
    }

    @Override
    public void onGotAppVersion(String appVersion) {
        Log.d(TAG, "appversion local: " + Commons.getAppVersion(this));
        Log.d(TAG, "appversion globa: " + appVersion);

        if (!Commons.getAppVersion(this).equals(appVersion)) {
            startActivity(new Intent(this, UpdateActivity.class));
            finish();

        } else {
            if (!isListenerSet) {
                fireBaseHandler.setWinnersListener(this, FirebaseAuth.getInstance().getCurrentUser().getUid(), this);
                isListenerSet = true;
            }
        }
    }

    public class AutoScroller extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (iteration != 0) {
                        if (page >= sliderItems.size()) {
                            page = 0;
                            advertisementPager.setCurrentItem(0);
                        } else {
                            advertisementPager.setCurrentItem(page);
                        }
                        page++;
                    }
                    iteration++;
                }
            });
        }
    }

}

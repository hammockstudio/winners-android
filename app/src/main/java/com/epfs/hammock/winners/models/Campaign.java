package com.epfs.hammock.winners.models;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 6/15/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Campaign {
    private String campaignId;
    private String campaignName;

    private float campaignFrom;

    private String aboutYou;

    private Object goldDiscount;
    private Object goldDescription;

    private float lng;

    private Object remainingVouchers;

    private String logoImageUrl;

    private String categoryName;

    private String promoCodes;

    private List<Claim> uids;

    private boolean epfsDiscount;
    private boolean freeDiscount;

    private String details;


    private Object bronzeDiscount;
    private Object bronzeDescription;

    private float lat;

    private Object silverDiscount;
    private Object silverDescription;

    private String vendorContactNumber;

    private String vendorName;

    private String status;

    private String categoryId;

    private Object voucherCount;

    private String vendorId;

    private Object cost;

    private String termsAndConditions;

    private Object discount;

    private List<Images> images;

    private String coverImageUrl;

    private Object numberOfVouchersSold;

    private String coverImageRef;

    private float campaignTo;

    private Map<String, Long> impressions;

    public Object getHighestDiscount() {
        if (this.goldDiscount != null && !this.goldDiscount.equals(""))
            return this.goldDiscount;
        if (this.silverDiscount != null && !this.silverDiscount.equals(""))
            return this.silverDiscount;
        else
            return this.bronzeDiscount;
    }


}



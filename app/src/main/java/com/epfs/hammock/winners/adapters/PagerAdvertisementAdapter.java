package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.DealActivity;
import com.epfs.hammock.winners.activities.Favourites;
import com.epfs.hammock.winners.models.SliderItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 6/9/17.
 */

public class PagerAdvertisementAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    //    int[] images;
    List<SliderItem> sliderItems = new ArrayList<>();

    public PagerAdvertisementAdapter(Context context, List<SliderItem> sliderItems) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sliderItems = sliderItems;
    }

    @Override
    public int getCount() {
        return sliderItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.listitem_advertisement, container, false);
        Picasso.with(mContext).load(sliderItems.get(position).getSliderImageUrl()).into((ImageView) itemView.findViewById(R.id.imgAdvertisement));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sliderCampaignId = sliderItems.get(position).getCampaignId();
                String sliderUrl = sliderItems.get(position).getSliderLink();



                if(sliderUrl!=null && sliderUrl.isEmpty()){
                    if(sliderCampaignId!=null) {
                        if(!sliderItems.get(position).getCampaignId().isEmpty()) {
                            Intent intent = new Intent(mContext, DealActivity.class);
                            intent.putExtra("campaignId", sliderItems.get(position).getCampaignId());
                            mContext.startActivity(intent);
                        }
                    }
                }else{
                    if(sliderUrl!=null && sliderUrl.startsWith("http")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.guessUrl(sliderUrl)));
                        mContext.startActivity(browserIntent);
                    }
                }


            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

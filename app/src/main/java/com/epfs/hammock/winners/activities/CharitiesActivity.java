package com.epfs.hammock.winners.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.CharitiesAdapter;

import java.util.List;

public class CharitiesActivity extends AppCompatActivity {

    int[] charities = {R.drawable.charities_1,
            R.drawable.charities_2,
            R.drawable.charities_3,
            R.drawable.charities_4,
            R.drawable.charities_5,
            R.drawable.charities_6,
            R.drawable.charities_8,
            R.drawable.charities_9,
            R.drawable.charities_10,
            R.drawable.charities_11,
            R.drawable.charities_12,
            R.drawable.charities_13,
            R.drawable.charities_14,
            R.drawable.charities_15,
            R.drawable.charities_16,
            R.drawable.charities_17,
            R.drawable.charities_18

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charities);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.charities);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView charityList = (RecyclerView) findViewById(R.id.charityList);
        CharitiesAdapter charitiesAdapter = new CharitiesAdapter(charities,this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        charityList.setLayoutManager(mLayoutManager);
        charityList.setItemAnimator(new DefaultItemAnimator());
        charityList.setAdapter(charitiesAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
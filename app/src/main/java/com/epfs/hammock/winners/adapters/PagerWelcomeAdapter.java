package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.epfs.hammock.winners.R;

/**
 * Created by allan on 6/9/17.
 */

public class PagerWelcomeAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    String[] colours;

    public PagerWelcomeAdapter(Context context, String[] colours) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.colours = colours;
    }

    @Override
    public int getCount() {
        return colours.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = null;
        switch (position) {
//            case 0:
//                itemView = mLayoutInflater.inflate(R.layout.listitem_pager_item_0, container, false);
//                break;
//            case 1:
//                itemView = mLayoutInflater.inflate(R.layout.listitem_pager_item, container, false);
//                break;
//            case 2:
//                itemView = mLayoutInflater.inflate(R.layout.listitem_pager_item_two, container, false);
//                break;
            case 0:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_1, container, false);
                break;
            case 1:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_2, container, false);
                break;
            case 2:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_3, container, false);
                break;
            case 3:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_4, container, false);
                break;
            case 4:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_5, container, false);
                break;
            case 5:
                itemView = mLayoutInflater.inflate(R.layout.onboarding_screen_6, container, false);
                break;
            default:
                itemView = mLayoutInflater.inflate(R.layout.listitem_pager_item_two, container, false);
                break;
        }
//        itemView.findViewById(R.id.bgColor).setBackgroundColor(Color.parseColor(colours[position]));
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

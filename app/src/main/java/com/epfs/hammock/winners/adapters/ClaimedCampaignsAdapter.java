package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.models.Campaign;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class ClaimedCampaignsAdapter extends RecyclerView.Adapter<ClaimedCampaignsAdapter.ViewHolder> {
    private List<Campaign> items;
    private static RecyclerViewClickListener itemListener;
    private Context mContext;

    public ClaimedCampaignsAdapter(List<Campaign> items, RecyclerViewClickListener itemListener, Context context) {
        this.items = items;
        this.itemListener = itemListener;
        this.mContext = context;
    }

    public void setData(List<Campaign> items) {
        this.items = items;
    }

    public List<Campaign> getData() {
        return items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_deal_smallview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Campaign item = items.get(position);
        try {
            holder.txtTitle.setText(item.getCampaignName());
            holder.txtLongDesc.setText(Html.fromHtml(item.getDetails()));

            String discount = item.getHighestDiscount().toString();
            if(!discount.equals("0"))
                holder.txtDiscount.setText("Up to "+discount + "% off");
            else
                holder.txtDiscount.setText("Special Offer");


            if (item.getImages()!=null)
                Picasso.with(mContext).load(item.getImages().get(0).getImageUrl()).into(holder.imgPromotion);
            else
                holder.imgPromotion.setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder));
            if (Account.winnersPerson.getCampaignsFavourites().contains(item.getCampaignId())) {
                holder.indicatorFavourite.setVisibility(View.VISIBLE);
            } else {
                holder.indicatorFavourite.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Log.d("Exception onBind", ex.toString());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public ImageView imgPromotion;
        public TextView txtTitle;
        public TextView txtLongDesc;
        public TextView txtDiscount;
        public ImageView indicatorFavourite;

        public ViewHolder(View itemView) {
            super(itemView);
            imgPromotion = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            indicatorFavourite = (ImageView) itemView.findViewById(R.id.indicatorFavourite);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtLongDesc = (TextView) itemView.findViewById(R.id.txtLongDesc);
            txtDiscount = (TextView) itemView.findViewById(R.id.txtDiscount);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            itemListener.recyclerViewListLongClicked(view, this.getAdapterPosition());
            return true;
        }
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);

        void recyclerViewListLongClicked(View v, int position);

    }
}

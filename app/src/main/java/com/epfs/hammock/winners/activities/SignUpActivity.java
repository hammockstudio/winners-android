package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Person;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignUpActivity extends AppCompatActivity implements FireBaseHandler.UserRegisterUpdate {

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;
    private boolean validation = true;

    FireBaseHandler fireBaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        fireBaseHandler = new FireBaseHandler(this);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();

//        ((EditText) findViewById(R.id.txtUsername)).setText("Steve");
//        ((EditText) findViewById(R.id.txtEmail)).setText("allan.barthelot@gmail.com");
//        ((EditText) findViewById(R.id.txtPassword)).setText("password");
//        ((EditText) findViewById(R.id.txtRPassword)).setText("password");

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });

        findViewById(R.id.btnCreateAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation = true;
                String username = ((EditText) findViewById(R.id.txtUsername)).getText().toString().trim();
                String email = ((EditText) findViewById(R.id.txtEmail)).getText().toString().trim();
                String password = ((EditText) findViewById(R.id.txtPassword)).getText().toString().trim();
                String repPassword = ((EditText) findViewById(R.id.txtRPassword)).getText().toString().trim();

                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please enter your email", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please enter your password", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                if(!repPassword.equals(password)){
                    Toast.makeText(getApplicationContext(), "Your passwords do not match. Please check and try again.", Toast.LENGTH_LONG).show();
                    validation = false;
                }

                if (validation) {
                    progressDialog.setMessage("Signing you up, please wait...");
                    progressDialog.show();
                    createUser(username,email, password);
                }
            }
        });
    }

    private void createUser(final String username, String email, String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            handleUser(firebaseAuth.getCurrentUser(), username);
                        } else {
                            String message = "";
                            if(task.getException().getMessage() != null){
                                message = task.getException().getMessage();
                            }else{
                                message = task.getException().toString();
                            }


                            new AlertDialog.Builder(new ContextThemeWrapper(SignUpActivity.this, R.style.myDialog))
                                    .setMessage(message)
                                    .show();
                        }
                        progressDialog.dismiss();
                    }
                });
    }

    private void handleUser(FirebaseUser firebaseUser, String username){
        if(firebaseUser != null){
            Account.winnersUser = firebaseUser;
            fireBaseHandler.registerUser(firebaseUser, this, username);
        }
    }

    @Override
    public void onUserRegistered(Person person, boolean isUserExists) {
        Account.winnersPerson = person;
        Intent intent = new Intent(getApplicationContext(), HomePageActivity.class);
        intent.putExtra("fromReg", true);
        startActivity(intent);
        finish();

    }
}

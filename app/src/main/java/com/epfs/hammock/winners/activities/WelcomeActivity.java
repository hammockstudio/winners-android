package com.epfs.hammock.winners.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.PagerWelcomeAdapter;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Timer;
import java.util.TimerTask;

import static com.epfs.hammock.winners.constants.Account.winnersUser;


public class WelcomeActivity extends AppCompatActivity {
    int page = 0;
    int seconds = 2;
    String[] colors = {"", "", "", "", "", ""};
    ViewPager bgPager;
    Timer timer;

    private FirebaseAuth mAuth;

    boolean switched = false;

    FireBaseHandler fireBaseHandler;

    String TAG = "welcomepage";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_welcome);
        fireBaseHandler = new FireBaseHandler(this);

        bgPager = (ViewPager) findViewById(R.id.bgPager);
        bgPager.setOnTouchListener(null);
        PagerWelcomeAdapter pagerAdapter = new PagerWelcomeAdapter(this, colors);
        bgPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_dots);
        tabLayout.setupWithViewPager(bgPager, true);

        timer = new Timer();
        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            }
        });
        findViewById(R.id.btnSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, SignUpActivity.class));
            }
        });
        findViewById(R.id.btnSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchControls();
            }
        });
        findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentPage = bgPager.getCurrentItem() + 1;

                if (currentPage < colors.length) {
                    bgPager.setCurrentItem(bgPager.getCurrentItem() + 1);
                }
            }
        });

        bgPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position + 1 >= colors.length && !switched)
                    switchControls();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    protected void onPause() {
        super.onPause();
//        timer.cancel();

    }

    @Override
    protected void onResume() {
        super.onResume();
//            timer = new Timer();
//            timer.scheduleAtFixedRate(new AutoScroller(), 0, seconds * 1000);
    }

    @Override
    public void onStart() {
        super.onStart();
        handleLogin(mAuth.getCurrentUser());

    }

    void switchControls() {
        switched = true;
        final Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_slow);
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out_slow);

        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                findViewById(R.id.lytOnboarding).setVisibility(View.INVISIBLE);
                findViewById(R.id.lytLoginSignup).startAnimation(fadeInAnimation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                findViewById(R.id.lytLoginSignup).setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        findViewById(R.id.lytOnboarding).startAnimation(fadeOutAnimation);
    }

    private void handleLogin(FirebaseUser currentUser) {
        if (currentUser == null) {

        } else {
            winnersUser = currentUser;
            Intent intent = new Intent(WelcomeActivity.this, HomePageActivity.class);
            intent.putExtra("fromReg", false);
            if (getIntent().getStringArrayListExtra("notificationCampaignId") != null) {
                intent.putExtra("notificationCampaignId", getIntent().getStringArrayListExtra("notificationCampaignId"));
            }
            startActivity(intent);
            finish();
        }
    }

    class AutoScroller extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (page >= colors.length) {
                        page = 0;
                        bgPager.setCurrentItem(0);
                    } else {
                        bgPager.setCurrentItem(page);
                    }
                    Log.d("timer", String.valueOf(page));
                    page++;
                }
            });
        }
    }
}



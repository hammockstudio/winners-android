package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Person;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

//import static com.example.hammock.winners.constants.Account.winnersUser;

public class LoginActivity extends AppCompatActivity implements FireBaseHandler.UserRegisterUpdate {
    CallbackManager callbackManager;
    private FirebaseAuth firebaseAuth;
    String TAG = "loginactivity";

    LoginButton loginButton;

    ProgressDialog progressDialog;

    EditText txtEmail;

    boolean validation = true;

    ContextThemeWrapper contextThemeWrapper;

    FireBaseHandler fireBaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebaseAuth = FirebaseAuth.getInstance();
        fireBaseHandler = new FireBaseHandler(this);

        progressDialog = new ProgressDialog(this);
        contextThemeWrapper = new ContextThemeWrapper(this, R.style.myDialog);

//        ((EditText) findViewById(R.id.txtEmail)).setText("allan.barthelot@gmail.com");
//        ((EditText) findViewById(R.id.txtPassword)).setText("password");

        txtEmail = (EditText) findViewById(R.id.txtEmail);


        findViewById(R.id.btnSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        loginButton = (LoginButton) findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                progressDialog.setMessage("Logging you in...");
                progressDialog.show();
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

        (findViewById(R.id.btnLogin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation = true;
                String email = txtEmail.getText().toString().trim();
                String password = ((EditText) findViewById(R.id.txtPassword)).getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please enter email", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_LONG).show();
                    validation = false;
                }

                if (validation) {
                    progressDialog.setMessage("Logging you in...");
                    progressDialog.show();
                    loginUser(email, password);
                }
            }
        });

        (findViewById(R.id.btnForgotPassword)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = txtEmail.getText().toString().trim();
                if (Commons.isEmailValid(email)) {
                    new AlertDialog.Builder(contextThemeWrapper)
                            .setMessage("We will send a link to reset your password with to the email you've specified. Continue?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    forgotPassword(email);

                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                } else
                    Toast.makeText(getApplicationContext(), "Your email address is invalid", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        handleLogin(firebaseAuth.getCurrentUser());

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            handleLogin(user);
                        } else {
                            Toast.makeText(LoginActivity.this, task.getException().getMessage(),
                                    Toast.LENGTH_LONG).show();
                            LoginManager.getInstance().logOut();
                            progressDialog.dismiss();


                            handleLogin(null);
                        }
                    }
                });
    }

    private void handleLogin(FirebaseUser currentUser) {
        if (currentUser == null)
            Log.d(TAG, "Not logged in");
        else {
            Account.winnersUser = currentUser;
            fireBaseHandler.registerUser(currentUser, this, null);
        }
    }

    //email/pass login
    private void loginUser(String email, String password) {
        Log.d(TAG, email + password);
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            handleLogin(firebaseAuth.getCurrentUser());

                        } else
                            Log.d(TAG, "nope" + task.getException());
                    }
                });
    }

    private void forgotPassword(String email) {
        progressDialog.setMessage("Sending reset link to your mail..");
        progressDialog.show();
        firebaseAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            new AlertDialog.Builder(contextThemeWrapper)
                                    .setMessage("The email has been sent! Please check your mail.")
                                    .show();
                        } else {
                            new AlertDialog.Builder(contextThemeWrapper)
                                    .setMessage("There was an error. Sure the email address specified is correct?")
                                    .show();
                        }
                    }
                });
    }

    @Override
    public void onUserRegistered(Person person, boolean isUserExists) {
        Account.winnersPerson = person;
        progressDialog.dismiss();

        Intent intent = new Intent(LoginActivity.this, HomePageActivity.class);
        intent.putExtra("fromReg", !isUserExists);
        startActivity(intent);
        finish();
    }
}

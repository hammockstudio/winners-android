package com.epfs.hammock.winners.constants;

import com.epfs.hammock.winners.models.Person;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by allan on 6/16/17.
 */

public class Account {
    public static FirebaseUser winnersUser;
    public static Person winnersPerson;
    public static void logUserOut(){
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        winnersUser = null;
        winnersPerson = null;
    }
}

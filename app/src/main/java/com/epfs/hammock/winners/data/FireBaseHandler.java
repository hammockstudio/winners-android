package com.epfs.hammock.winners.data;

import android.content.Context;
import android.util.Log;

import com.epfs.hammock.winners.activities.PreferencesActivity;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.constants.Fields;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Campaign;
import com.epfs.hammock.winners.models.Category;
import com.epfs.hammock.winners.models.Claim;
import com.epfs.hammock.winners.models.Person;
import com.epfs.hammock.winners.models.SliderItem;
import com.epfs.hammock.winners.models.Vendor;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by allan on 6/15/17.
 */

public class FireBaseHandler {
    String TAG = "firebaseHandler";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabase = database.getReference();

    //    public static List<Campaign> campaignItems = new ArrayList<>();
    public static List<Category> categoryItems = new ArrayList<>();
    public static HashMap<String, Integer> categoryPriority = new HashMap<>();
    private Context mContext;

    public FireBaseHandler() {
    }

    public FireBaseHandler(Context context) {
        this.mContext = context;
    }

    public void getLatestAppVersion(final HomeScreenUpdate homeScreenUpdate) {
        DatabaseReference appRef = FirebaseDatabase.getInstance().getReference().child(Fields.appversion).child("versionCode");
        appRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                homeScreenUpdate.onGotAppVersion(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getAdvertisements(final HomeScreenUpdate homeScreenUpdate) {
        DatabaseReference slideRef = FirebaseDatabase.getInstance().getReference().child(Fields.slider);
        slideRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<SliderItem> sliderItems = new ArrayList<SliderItem>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    sliderItems.add(snapshot.getValue(SliderItem.class));
                }
                homeScreenUpdate.onSliderUpdate(sliderItems);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCategoriesListener(final HomeScreenUpdate homeScreenUpdate) {
        final HashMap<String, Integer> counts = new HashMap<>();
        FirebaseDatabase.getInstance().getReference().child(Fields.campaigns).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    if (counts.get(child.child("categoryId").getValue().toString()) == null)
                        counts.put(child.child("categoryId").getValue().toString(), 0);

                    counts.put(child.child("categoryId").getValue().toString(), (counts.get(child.child("categoryId").getValue().toString())) + 1);
                }

                DatabaseReference catRef = FirebaseDatabase.getInstance().getReference().child(Fields.categories);
                catRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        categoryItems.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Category category = snapshot.getValue(Category.class);
                            category.setCategoryId(snapshot.getKey().toString());

                            if (counts.get(snapshot.getKey().toString()) != null)
                                category.setCounter(counts.get(snapshot.getKey().toString()));
                            else
                                category.setCounter(0);

                            categoryItems.add(category);
                        }
                        Collections.sort(categoryItems);
                        homeScreenUpdate.onCategoriesUpdate(categoryItems);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void setWinnersListener(final Context mContext, final String uId, final HomeScreenUpdate homeScreenUpdate) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(Fields.users).child(uId);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Account.winnersPerson = dataSnapshot.getValue(Person.class);
                homeScreenUpdate.onHomeUpdate();
                homeScreenUpdate.onAccountUpdate(dataSnapshot.getValue(Person.class));
                Log.d(TAG, "User changed");

            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }


    public void getCampaigns(final Context mContext, final List<String> categoryIds, final List<String> merchantIds, final String uId, final int max, final OnCampaignsRecieved onCampaignsRecieved) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Campaign> campaignItems = new ArrayList<Campaign>();
                DataSnapshot campaigns = dataSnapshot.child(Fields.campaigns);
                for (DataSnapshot snapshot : campaigns.getChildren()) {

                    Campaign campaignItem = getCompleteCampaign(snapshot.getValue(Campaign.class), snapshot);

                    boolean validCategory = true;
                    boolean validVendor = true;

                    if (categoryIds != null && categoryIds.size() > 0)
                        validCategory = categoryIds.contains(campaignItem.getCategoryId());
                    if (merchantIds != null && merchantIds.size() > 0)
                        validVendor = merchantIds.contains(campaignItem.getVendorId());

                    if (validCategory && validVendor)
                        addCampaignToList(campaignItem, campaignItems);

                    if (campaignItems.size() > max)
                        break;
                }
                Log.d(TAG, "DB changed");
                onCampaignsRecieved.onCampaignsRecieved(campaignItems);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void getCampaignByIds(final List<String> campaignIds, final OnCampaignsRecieved onCampaignsRecieved) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Campaign> validCampaigns = new ArrayList<>();
                for (String campaignId : campaignIds) {
                    if (campaignId != null) {
                        DataSnapshot campaign = dataSnapshot.child(Fields.campaigns).child(campaignId);
                        if (campaign.getValue() != null)
                            addCampaignToList(getCompleteCampaign(campaign.getValue(Campaign.class), campaign), validCampaigns);
                    }
                }
                onCampaignsRecieved.onCampaignsRecieved(validCampaigns);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private Campaign getCompleteCampaign(Campaign campaign, DataSnapshot snapshot) {
        if (campaign != null)
            campaign.setCampaignId(snapshot.getKey());
        return campaign;
    }

    private void addCampaignToList(Campaign campaign, List<Campaign> campaigns) {
        if (campaign.getStatus().toString().equals("1") &&
                Commons.getCurrentTimeEpoch() > campaign.getCampaignFrom() &&
                Commons.getCurrentTimeEpoch() < campaign.getCampaignTo())
            campaigns.add(campaign);
    }

    public void getCampaignById(String campaignId, final OnCampaignsRecieved onCampaignsRecieved) {
        DatabaseReference campaignRef = FirebaseDatabase.getInstance().getReference().child(Fields.campaigns).child(campaignId);
        campaignRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Campaign> campaigns = new ArrayList<>();
                campaigns.add(getCompleteCampaign(dataSnapshot.getValue(Campaign.class), dataSnapshot));
                onCampaignsRecieved.onCampaignsRecieved(campaigns);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getMerchantByIds(final List<String> merchantIds, final OnVendorsRecieved onVendorsRecieved) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Vendor> validVendors = new ArrayList<>();
                for (String merchantId : merchantIds) {
                    if (merchantId != null) {
                        DataSnapshot vendor = dataSnapshot.child(Fields.vendors).child(merchantId);
                        if (vendor != null)
                            validVendors.add(getCompleteVendor(vendor.getValue(Vendor.class), vendor));
                    }
                }
                onVendorsRecieved.onVendorsRecieved(validVendors);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void getMerchantById(String merchantId, final OnVendorsRecieved onVendorsRecieved) {
        DatabaseReference campaignRef = FirebaseDatabase.getInstance().getReference().child(Fields.vendors).child(merchantId);
        campaignRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Vendor> vendors = new ArrayList<>();
                vendors.add(getCompleteVendor(dataSnapshot.getValue(Vendor.class), dataSnapshot));
                onVendorsRecieved.onVendorsRecieved(vendors);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void claimVoucher(final String campaignId, final String uId, final int transaction, final DealUpdate dealUpdate) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference campaignRef = mDatabase.child(Fields.campaigns)
                .child(campaignId);
        final DatabaseReference userRef = mDatabase.child(Fields.users)
                .child(uId);
//
        campaignRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Campaign campaign = mutableData.getValue(Campaign.class);
                if (campaign == null) {
                    Log.d("doTransaction", "campaign == null");
                    return Transaction.success(mutableData);
                }

                List<Claim> claims = campaign.getUids();
                if (claims == null) {
                    claims = new ArrayList<>();
                }
                if (transaction > 0)
                    claims.add(new Claim(uId, String.valueOf(Commons.getCurrentTimeEpoch())));
                else {
                    claims.remove(uId);
                    for (int i = 0; i < claims.size(); i++) {
                        if (claims.get(i).getUid().equals(uId)) {
                            claims.remove(i);
                            break;
                        }
                    }
                }

                int vouchersRemaining = Integer.parseInt(campaign.getRemainingVouchers().toString());
                int vouchersSold = Integer.parseInt(campaign.getNumberOfVouchersSold().toString());
                int vouchersTotal = Integer.parseInt(campaign.getVoucherCount().toString());

                if (vouchersRemaining >= 1 && vouchersSold <= vouchersTotal) {
                    mutableData.child("numberOfVouchersSold").setValue(String.valueOf(Integer.parseInt(campaign.getNumberOfVouchersSold().toString()) + transaction));
                    mutableData.child("remainingVouchers").setValue(String.valueOf(Integer.parseInt(campaign.getRemainingVouchers().toString()) - transaction));
                    mutableData.child("uids").setValue(claims);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                userRef.runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        if (mutableData == null) {
                            return Transaction.success(mutableData);
                        }
                        Person person = mutableData.getValue(Person.class);
                        List<String> cIds = person.getCampaignsClaimed();
                        if (person.getCampaignsClaimed() == null) {
                            cIds = new ArrayList<>();
                        }
                        if (transaction > 0) {
                            if (!cIds.contains(campaignId))
                                cIds.add(campaignId);
                        } else
                            cIds.remove(campaignId);

                        person.setCampaignsClaimed(cIds);
                        mutableData.setValue(person);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                        getCampaignById(campaignId, new OnCampaignsRecieved() {
                            @Override
                            public void onCampaignsRecieved(List<Campaign> campaigns) {
                                dealUpdate.onClaimed(campaigns.get(0), true);
                            }
                        });
                    }
                });
            }
        });
    }

    public void setCampaignsSeen(List<String> campaignIds, final String uId) {
        for (final String campaignId : campaignIds) {
            final DatabaseReference campaignRef = mDatabase.child(Fields.campaigns)
                    .child(campaignId);
            campaignRef.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    if (mutableData == null) {
                        return Transaction.success(mutableData);
                    }

                    mutableData.child("impressions").child(uId).setValue(Commons.getCurrentTimeEpoch());
                    return Transaction.success(mutableData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onComplete: " + campaignId + " hidden from next time");
                }
            });
        }
    }

    public void registerUser(final FirebaseUser firebaseUser, final UserRegisterUpdate userRegisterUpdate, final String userName) {

        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child(Fields.users);
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String username;
                if (userName != null) {
                    username = userName;
                } else {
                    username = firebaseUser.getDisplayName();
                }

                String imageUrl = "";
                if (firebaseUser.getPhotoUrl() != null)
                    imageUrl = firebaseUser.getPhotoUrl().toString();


                boolean isUserExist = dataSnapshot.hasChild(firebaseUser.getUid());
                if (!isUserExist) {
                    Person person = new Person(firebaseUser.getUid(),
                            username, firebaseUser.getEmail(), imageUrl, "2", "0",
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            new ArrayList<String>(),
                            true, true);
                    rootRef.child(firebaseUser.getUid()).setValue(person);
                    userRegisterUpdate.onUserRegistered(person, isUserExist);

                } else
                    userRegisterUpdate.onUserRegistered(dataSnapshot.child(firebaseUser.getUid()).getValue(Person.class), isUserExist);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void switchCategoryPreference(final String categoryId, String uId, final boolean add) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                .child(Fields.users).child(uId);
        userRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Person person = mutableData.getValue(Person.class);
                List<String> categories = new ArrayList<>();
                if (person.getCategories() == null) {
                    person.setCategories(categories);
                }
                categories = person.getCategories();

                if (add) {
                    if (!categories.contains(categoryId)) {
                        categories.add(categoryId);
                    }
                } else {
                    if (categories.contains(categoryId)) {
                        categories.remove(categoryId);
                    }
                }

                person.setCategories(categories);
                mutableData.setValue(person);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                ((PreferencesActivity) mContext).initList(false);
            }
        });
    }

    public void switchFavourite(final String campaignId, String uId, final boolean add, final DealUpdate dealUpdate) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                .child(Fields.users).child(uId);
        userRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Person person = mutableData.getValue(Person.class);
                List<String> favourites = new ArrayList<>();
                if (person.getCampaignsFavourites() == null) {
                    person.setCampaignsFavourites(favourites);
                }
                favourites = person.getCampaignsFavourites();

                if (add) {
                    if (!favourites.contains(campaignId)) {
                        favourites.add(campaignId);
                    }
                } else {
                    if (favourites.contains(campaignId)) {
                        favourites.remove(campaignId);
                    }
                }
                person.setCampaignsFavourites(favourites);
                mutableData.setValue(person);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                dealUpdate.onFavourited(add);
            }
        });
    }

    public void saveSettings(String uId, final boolean recieveNotification, final boolean sendGeo, final SettingsUpdate settingsUpdate) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference()
                .child(Fields.users).child(uId);
        userRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Person person = mutableData.getValue(Person.class);
                person.setRecieveNotification(recieveNotification);
                person.setSendGeo(sendGeo);

                mutableData.setValue(person);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                settingsUpdate.onSettingsUpdate(dataSnapshot.getValue(Person.class));
            }
        });
    }


    private Vendor getCompleteVendor(Vendor vendor, DataSnapshot snapshot) {
        if (vendor != null)
            vendor.setVendorId(snapshot.getKey());
        return vendor;
    }


    public interface OnCampaignsRecieved {
        void onCampaignsRecieved(List<Campaign> campaigns);
    }

    public interface OnVendorsRecieved {
        void onVendorsRecieved(List<Vendor> vendors);
    }

    public interface DealUpdate {
        void onFavourited(boolean favourite);

        void onClaimed(Campaign campaign, boolean updateClaimed);
    }

    public interface SettingsUpdate {
        void onSettingsUpdate(Person person);
    }

    public interface HomeScreenUpdate {
        void onAccountUpdate(Person person);

        void onCategoriesUpdate(List<Category> categories);

        void onHomeUpdate();

        void onSliderUpdate(List<SliderItem> sliderItems);

        void onGotAppVersion(String appVersion);
    }

    public interface UserRegisterUpdate {
        void onUserRegistered(Person person, boolean exists);
    }
}

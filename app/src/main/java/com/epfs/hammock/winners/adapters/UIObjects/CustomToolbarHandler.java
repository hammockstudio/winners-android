package com.epfs.hammock.winners.adapters.UIObjects;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.ClaimedCampaigns;
import com.epfs.hammock.winners.activities.DealActivity;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.geo.GeoListener;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Campaign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.google.android.gms.internal.zzagy.runOnUiThread;

/**
 * Created by allan on 7/7/17.
 */

public class CustomToolbarHandler implements GeoListener.OnGeoRecieved {
    LinearLayout toolbar;
    Context context;
    boolean showNearby = true;


    public CustomToolbarHandler(final Context context, LinearLayout toolbar, boolean showNearby) {
        this.context = context;
        this.toolbar = toolbar;
        this.showNearby = showNearby;
        if (showNearby) {
            GeoListener.setGeoNearbyListener(this);
            setNearbyCount(GeoListener.nearbyCampaigns.size());

        }

        (this.toolbar.findViewById(R.id.btnClaims)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ClaimedCampaigns.class));
            }
        });

    }

    public void setToolBar(LinearLayout toolbar) {
        this.toolbar = toolbar;
    }

    public void getToolBar() {

    }

    public void setNearbyCount(final int size) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("CustomToolbarHandler", "run: " + size);
                if (size > 0) {
                    ((TextView) toolbar.findViewById(R.id.txtNearbyCount)).setText(String.valueOf(size));
                    (toolbar.findViewById(R.id.lytNearbyCount)).setVisibility(View.VISIBLE);

                } else {
                    (toolbar.findViewById(R.id.lytNearbyCount)).setVisibility(View.INVISIBLE);
                }
            }
        });


    }

    public void setHomeAsBack() {
        ((ImageView) toolbar.findViewById(R.id.btnDrawer)).setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_back));
        toolbar.findViewById(R.id.btnDrawer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof DealActivity) {
                    ((DealActivity) context).finish();
                }
            }
        });
    }

    public void hideHomeButtons(String title) {
        (toolbar.findViewById(R.id.btnHome)).setVisibility(View.GONE);
        (toolbar.findViewById(R.id.btnHomeIndicator)).setVisibility(View.GONE);
        (toolbar.findViewById(R.id.btnCategories)).setVisibility(View.GONE);
        (toolbar.findViewById(R.id.btnCategoriesIndicator)).setVisibility(View.GONE);
        (toolbar.findViewById(R.id.btnMap)).setVisibility(View.GONE);

        (toolbar.findViewById(R.id.txtPageTitle)).setVisibility(View.VISIBLE);
        ((TextView) toolbar.findViewById(R.id.txtPageTitle)).setText(title);
    }


    @Override
    public void onGeoRecieved(List<Campaign> nearbyCampaigns) {
        FireBaseHandler fireBaseHandler = new FireBaseHandler();
        HashMap<String, ArrayList<String>> hashMap = new HashMap<>();
        hashMap.put("seen", new ArrayList<String>());
        hashMap.put("unseen", new ArrayList<String>());

        for (Campaign campaign : nearbyCampaigns) {
            if (campaign.getImpressions() != null && campaign.getImpressions().containsKey(Account.winnersPerson.getPersonId())) {
                Long time = campaign.getImpressions().get(Account.winnersPerson.getPersonId());
                if ((Commons.getCurrentTimeEpoch() - time > 604800000))
                    hashMap.get("unseen").add(campaign.getCampaignId());
                else
                    hashMap.get("seen").add(campaign.getCampaignId());

            } else
                hashMap.get("unseen").add(campaign.getCampaignId());
        }

        GeoListener.setNotification(context, hashMap.get("unseen"));
        fireBaseHandler.setCampaignsSeen(hashMap.get("unseen"), Account.winnersPerson.getPersonId());
        if (this.showNearby)
            setNearbyCount(nearbyCampaigns.size());
    }

}

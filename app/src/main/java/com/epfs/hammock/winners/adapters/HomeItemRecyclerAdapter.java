package com.epfs.hammock.winners.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.activities.DealActivity;
import com.epfs.hammock.winners.models.Campaign;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by allan on 6/2/17.
 */

public class HomeItemRecyclerAdapter extends RecyclerView.Adapter<HomeItemRecyclerAdapter.ViewHolder> {
    private List<Campaign> items;
    private Context mContext;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgHomeListItem;
        private TextView txtMainItemName;
        public ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            imgHomeListItem = (ImageView) itemView.findViewById(R.id.imgPromoIcon);
            txtMainItemName = (TextView) itemView.findViewById(R.id.txtTitle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }

    }

    public HomeItemRecyclerAdapter(List<Campaign> items, Context context) {
        this.items = items;
        this.mContext = context;
    }

    @Override
    public HomeItemRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listitem_home_item_item, parent, false);
        return new HomeItemRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HomeItemRecyclerAdapter.ViewHolder holder, int position) {
        final Campaign item = items.get(position);
        if (item.getImages() != null && item.getImages().get(0).getImageUrl() != null)
            Picasso.with(mContext).load(item.getImages().get(0).getImageUrl()).into(holder.imgHomeListItem, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
        else {
            holder.imgHomeListItem.setImageDrawable(mContext.getResources().getDrawable(R.drawable.placeholder));
            holder.progressBar.setVisibility(View.GONE);
        }
        holder.txtMainItemName.setText(item.getCampaignName());

        holder.imgHomeListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DealActivity.class);
                intent.putExtra("campaignId", item.getCampaignId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

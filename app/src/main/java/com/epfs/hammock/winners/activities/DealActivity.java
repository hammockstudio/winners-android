package com.epfs.hammock.winners.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.epfs.hammock.winners.R;
import com.epfs.hammock.winners.adapters.PagerAdvertisementAdapter;
import com.epfs.hammock.winners.adapters.UIObjects.CustomToolbarHandler;
import com.epfs.hammock.winners.adapters.UIObjects.PagerImagesAdapter;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.helpers.CircleTransform;
import com.epfs.hammock.winners.helpers.Commons;
import com.epfs.hammock.winners.models.Campaign;
import com.epfs.hammock.winners.models.Images;
import com.epfs.hammock.winners.models.Vendor;
import com.github.jinatonic.confetti.CommonConfetti;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DealActivity extends RootActivity implements FireBaseHandler.DealUpdate,
        FireBaseHandler.OnCampaignsRecieved {

    static final Integer CALL = 0x2;

    FireBaseHandler fireBaseHandler;

    MapView mapView;
    ProgressDialog progressDialog;

    String campaignId;

    Campaign thisCampaign;

    CustomToolbarHandler customToolbarHandler;

    ScrollView scrollView;

    LocationManager locationManager;

    String TAG = "DealActivity";

    //List<> images

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            setContentView(R.layout.activity_deal);

            this.overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);

            fireBaseHandler = new FireBaseHandler(this);
            progressDialog = new ProgressDialog(this);
            customToolbarHandler = new CustomToolbarHandler(this, (LinearLayout) findViewById(R.id.lytCustomToolbar), false);
            customToolbarHandler.setHomeAsBack();
            customToolbarHandler.hideHomeButtons("Deal");

            scrollView = (ScrollView) findViewById(R.id.lytLayout);
            scrollView.setVisibility(View.INVISIBLE);


            campaignId = getIntent().getStringExtra("campaignId");

            (findViewById(R.id.btnDrawer)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            (findViewById(R.id.btnClaim)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    claimThis();
                }
            });

            //lytWinnerDialog


            (findViewById(R.id.btnWishList)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setFavourite();
                }
            });
//
//            (findViewById(R.id.lytBronze)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    openMembership(0);
//                }
//            });
//            (findViewById(R.id.lytGold)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    openMembership(2);
//                }
//            });
//            (findViewById(R.id.lytSilver)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    openMembership(1);
//                }
//            });
//            (findViewById(R.id.lytEpfs)).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    openMembership(-99);
//                }
//            });

            (findViewById(R.id.btnCloseWinnerDialog)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showWinnerDialog(false);
                }
            });

            (findViewById(R.id.lytWinnerDialog)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showWinnerDialog(false);
                }
            });

            (findViewById(R.id.btnShare)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareClick();
                }
            });

            findViewById(R.id.lytVendorClaimed).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vendorClicked();
                }
            });

            findViewById(R.id.txtVendorName).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vendorClicked();
                }
            });

            mapView = (MapView) findViewById(R.id.deal_map);
            mapView.onCreate(savedInstanceState);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            fireBaseHandler.getCampaignById(campaignId, this);
        } catch (Exception ex) {
            Log.d("getCampaignById", "onResume: " + ex.toString());
        }

        if (Account.winnersPerson.getCampaignsClaimed() != null)
            fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsClaimed(), new FireBaseHandler.OnCampaignsRecieved() {
                @Override
                public void onCampaignsRecieved(List<Campaign> campaigns) {
                    setClaimCount(campaigns.size());
                }
            });
    }

    private void claimThis() {
        new AlertDialog.Builder(new ContextThemeWrapper(DealActivity.this, R.style.myDialog))
                .setMessage("Are you sure you want to claim this Deal?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        claimVoucher();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

//    private void openMembership(int membership) {
//        if (membership == -99)
//            claimThis();
//        else {
//            int membershipLevel = Integer.parseInt(Account.winnersPerson.getMembershipLevel());
//
//            if (membershipLevel >= membership) {
//                if (Account.winnersPerson.getCampaignsClaimed() != null) {
//                    if (!isClaimed())
//                        claimThis();
//                    else
//                        Toast.makeText(DealActivity.this, "You have already claimed this deal", Toast.LENGTH_SHORT).show();
//                }
//            } else {
//                new AlertDialog.Builder(new ContextThemeWrapper(DealActivity.this, R.style.myDialog))
//                        .setMessage("This is a discount only available to " + Commons.getMemberShip(membership) + " level users. You'll need to upgrade your account")
////                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                startActivity(new Intent(DealActivity.this, MembershipUpgradeActivity.class));
////                            }
////                        })
////                        .setNegativeButton("No", null)
//                        .show();
//            }
//        }
//    }

    private void setImageHeight() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        @SuppressLint("WrongViewCast") RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) findViewById(R.id.advertisement_lyt).getLayoutParams();
        params.height = (int) (((float) 9 / 16) * width);
        findViewById(R.id.advertisement_lyt).setLayoutParams(params);
    }

    private void setFavourite() {
        fireBaseHandler.switchFavourite(campaignId, Account.winnersUser.getUid(),
                !Account.winnersPerson.getCampaignsFavourites().contains(campaignId), this);
    }

    private void claimVoucher() {
        progressDialog.setMessage("Claiming..");
        progressDialog.show();
        fireBaseHandler.claimVoucher(campaignId, Account.winnersUser.getUid(), 1, this);
    }

    public void setCampaignItem(final Campaign campaignItem, boolean claimed) {
        thisCampaign = campaignItem;
        checkIfFavourite(campaignItem.getCampaignId());

//        if (campaignItem.getImages() != null && campaignItem.getImages().get(0).getImageUrl() != null)
//            Picasso.with(this).load(campaignItem.getImages().get(0).getImageUrl()).into((ImageView) findViewById(R.id.imgPromotion));
//        else
//            ((ImageView) findViewById(R.id.imgPromotion)).setImageDrawable(getResources().getDrawable(R.drawable.placeholder));

        PagerImagesAdapter pagerAdapter;
        if (campaignItem.getImages() != null) {
            TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_dots);
            tabLayout.setupWithViewPager(((ViewPager) findViewById(R.id.bgPager)), true);

            pagerAdapter = new PagerImagesAdapter(this, campaignItem.getImages());
        } else {
            ArrayList<Images> placeholder = new ArrayList<Images>();
            placeholder.add(new Images("", ""));
            pagerAdapter = new PagerImagesAdapter(this, placeholder);
        }
        ((ViewPager) findViewById(R.id.bgPager)).setAdapter(pagerAdapter);


        fireBaseHandler.getMerchantById(campaignItem.getVendorId(), new FireBaseHandler.OnVendorsRecieved() {
            @Override
            public void onVendorsRecieved(final List<Vendor> vendors) {
                if (vendors.get(0).getVendorImageUrl() != null)
                    Picasso.with(DealActivity.this).load(
                            vendors.get(0).getVendorImageUrl()).
                            into(((ImageView) findViewById(R.id.imgVendor)));
            }
        });


        ((TextView) findViewById(R.id.txtTitle)).setText(Html.fromHtml(campaignItem.getCampaignName()));
        ((TextView) findViewById(R.id.txtVendorName)).setText(Html.fromHtml(campaignItem.getVendorName()));
        ((TextView) findViewById(R.id.txtVendorNameClaimed)).setText(Html.fromHtml(campaignItem.getVendorName()));
        ((TextView) findViewById(R.id.txtAboutTitle)).setText("About " + Html.fromHtml(campaignItem.getVendorName()));
        ((TextView) findViewById(R.id.txtAbout)).setText(Html.fromHtml(campaignItem.getAboutYou()).toString().trim());
        ((TextView) findViewById(R.id.txtDetails)).setText(Html.fromHtml(campaignItem.getDetails()).toString().trim());
        ((TextView) findViewById(R.id.txtTerms)).setText(Html.fromHtml(campaignItem.getTermsAndConditions()).toString().trim());


        if (!campaignItem.isFreeDiscount()) {
            findViewById(R.id.lytMedals).setVisibility(View.VISIBLE);

            if (!campaignItem.isEpfsDiscount()) {
                findViewById(R.id.lytMedalsLevel).setVisibility(View.VISIBLE);
                findViewById(R.id.lytMedalsEpfs).setVisibility(View.GONE);
                if (campaignItem.getGoldDiscount() != null && !campaignItem.getGoldDiscount().toString().equals("")) {
                    ((TextView) findViewById(R.id.txtGoldDiscount)).setText(Html.fromHtml(campaignItem.getGoldDiscount().toString() + "%"));
                    ((TextView) findViewById(R.id.txtGoldDescription)).setText(Html.fromHtml(campaignItem.getGoldDescription().toString()));
                } else
                    findViewById(R.id.lytGold).setVisibility(View.GONE);

                if (campaignItem.getSilverDiscount() != null && !campaignItem.getSilverDiscount().toString().equals("")) {
                    ((TextView) findViewById(R.id.txtSilverDiscount)).setText(Html.fromHtml(campaignItem.getSilverDiscount().toString() + "%"));
                    ((TextView) findViewById(R.id.txtSilverDescription)).setText(Html.fromHtml(campaignItem.getSilverDescription().toString()));

                } else
                    findViewById(R.id.lytSilver).setVisibility(View.GONE);

                if (campaignItem.getBronzeDiscount() != null && !campaignItem.getBronzeDiscount().toString().equals("")) {
                    ((TextView) findViewById(R.id.txtBronzeDiscount)).setText(Html.fromHtml(campaignItem.getBronzeDiscount().toString() + "%"));
                    ((TextView) findViewById(R.id.txtBronzeDescription)).setText(Html.fromHtml(campaignItem.getBronzeDescription().toString()));
                } else
                    findViewById(R.id.lytBronze).setVisibility(View.GONE);
            } else {
                findViewById(R.id.lytMedalsLevel).setVisibility(View.GONE);
                findViewById(R.id.lytMedalsEpfs).setVisibility(View.VISIBLE);

                if (!campaignItem.getDiscount().toString().equals(""))
                    ((TextView) findViewById(R.id.txtEpfsDiscount)).setText(Html.fromHtml(campaignItem.getDiscount().toString() + "%"));
                else
                    findViewById(R.id.lytEpfs).setVisibility(View.GONE);
            }
        } else {
            findViewById(R.id.lytMedals).setVisibility(View.GONE);
        }


        ((TextView) findViewById(R.id.txtVouchersSold)).setText(String.format(getResources().getString(R.string.text_vouchers_sold),
                String.valueOf(campaignItem.getNumberOfVouchersSold())));

        if (campaignItem.getLat() == -1 && campaignItem.getLng() == -1) {
            mapView.setVisibility(View.GONE);
            findViewById(R.id.lytMap).setVisibility(View.GONE);
            findViewById(R.id.lytMapDetails).setVisibility(View.GONE);

        } else {
            findViewById(R.id.btnNavigate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String uri = "http://maps.google.com/maps?saddr=&daddr=" + thisCampaign.getLat() + "," + thisCampaign.getLng();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);
                }
            });


            try {
                setDistance(Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE) != 0);
            } catch (Exception ex) {
            }
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    Log.d("map", "ready");
                    LatLng position = new LatLng(campaignItem.getLat(), campaignItem.getLng());

                    googleMap.addMarker(new MarkerOptions()
                            .position(position).title(campaignItem.getCampaignName())
//                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                            .draggable(false).visible(true));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 17));
                    mapView.onResume();
                }
            });
            mapView.setVisibility(View.VISIBLE);
        }
        findViewById(R.id.lytLayout).setVisibility(View.VISIBLE);

        if (claimed) {
            progressDialog.dismiss();
            animateProjectile();
        }
        checkIfClaimed();

    }

    private void setClaimed(boolean claimed) {
        if (claimed) {
            int membershipLevel = Integer.parseInt(Account.winnersPerson.getMembershipLevel());
            (findViewById(R.id.btnClaim)).setVisibility(View.GONE);
//            (findViewById(R.id.lytMedals)).setVisibility(View.GONE);
            (findViewById(R.id.lytClaimStats)).setVisibility(View.GONE);
            (findViewById(R.id.lytWinner)).setVisibility(View.VISIBLE);
            (findViewById(R.id.lytWinnerFade)).setVisibility(View.VISIBLE);
            if (thisCampaign.getPromoCodes() != null && !thisCampaign.getPromoCodes().equals("")) {
                (findViewById(R.id.lytPromoCode)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.txtPromoCodes)).setText(Html.fromHtml(thisCampaign.getPromoCodes()));
            }else{
                (findViewById(R.id.lytPromoCode)).setVisibility(View.GONE);
            }

            findViewById(R.id.txtBronzeDescription).setVisibility(View.VISIBLE);
            findViewById(R.id.txtSilverDescription).setVisibility(View.VISIBLE);
            findViewById(R.id.txtGoldDescription).setVisibility(View.VISIBLE);

            findViewById(R.id.lytBronze).setVisibility(View.GONE);
            findViewById(R.id.lytSilver).setVisibility(View.GONE);
            findViewById(R.id.lytGold).setVisibility(View.GONE);

            if (!thisCampaign.isEpfsDiscount()) {
                switch (membershipLevel) {
                    case 0:
                        ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_bronze));
                        setDiscountDisplay(thisCampaign.getBronzeDiscount().toString());
                        findViewById(R.id.lytBronze).setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        if (thisCampaign.getSilverDiscount().toString().equals("")) {
                            ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_bronze));
                            setDiscountDisplay(thisCampaign.getBronzeDiscount().toString());
                            findViewById(R.id.lytBronze).setVisibility(View.VISIBLE);
                        } else {
                            ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_silver));
                            setDiscountDisplay(thisCampaign.getSilverDiscount().toString());
                            findViewById(R.id.lytSilver).setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        if (thisCampaign.getGoldDiscount().toString().equals("")) {
                            if (thisCampaign.getSilverDiscount().toString().equals("")) {
                                ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_bronze));
                                setDiscountDisplay(thisCampaign.getBronzeDiscount().toString());
                                findViewById(R.id.lytBronze).setVisibility(View.VISIBLE);
                            } else {
                                ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_silver));
                                setDiscountDisplay(thisCampaign.getSilverDiscount().toString());
                                findViewById(R.id.lytSilver).setVisibility(View.VISIBLE);
                            }
                        } else {
                            ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_gold));
                            setDiscountDisplay(thisCampaign.getGoldDiscount().toString());
                            findViewById(R.id.lytGold).setVisibility(View.VISIBLE);
                        }
                        break;
                }
            } else {
                ((ImageView) findViewById(R.id.imgMedalClaimed)).setImageDrawable(getResources().getDrawable(R.drawable.medal_green));
                setDiscountDisplay(thisCampaign.getDiscount().toString());
            }
        } else {

            findViewById(R.id.txtBronzeDescription).setVisibility(View.GONE);
            findViewById(R.id.txtSilverDescription).setVisibility(View.GONE);
            findViewById(R.id.txtGoldDescription).setVisibility(View.GONE);

            (findViewById(R.id.btnClaim)).setVisibility(View.VISIBLE);
            (findViewById(R.id.lytWinner)).setVisibility(View.GONE);
            (findViewById(R.id.lytWinnerFade)).setVisibility(View.GONE);
            if (!thisCampaign.isFreeDiscount())
                (findViewById(R.id.lytMedals)).setVisibility(View.VISIBLE);
            (findViewById(R.id.lytClaimStats)).setVisibility(View.VISIBLE);
            (findViewById(R.id.lytPromoCode)).setVisibility(View.GONE);
        }

    }

    private void setDiscountDisplay(String discount) {
        if (!discount.equals("0")) {
            ((TextView) findViewById(R.id.txtDiscountClaimed)).setText(discount + "%");
            findViewById(R.id.imgMedalClaimed).setVisibility(View.VISIBLE);
        } else {
            ((TextView) findViewById(R.id.txtDiscountClaimed)).setText("Special Offer");
            findViewById(R.id.imgMedalClaimed).setVisibility(View.GONE);
        }
    }

    private void setFavourited(boolean favourite) {
        if (favourite) {
            ((ImageView) findViewById(R.id.btnFavourite)).setImageDrawable(getResources().getDrawable(R.drawable.ic_star_selected));

        } else {
            ((ImageView) findViewById(R.id.btnFavourite)).setImageDrawable(getResources().getDrawable(R.drawable.ic_star));

        }
    }

    private void animateProjectile() {
        LinearLayout projectile = (LinearLayout) findViewById(R.id.projectile);
        projectile.setVisibility(View.VISIBLE);

        Animation bottomUp = AnimationUtils.loadAnimation(DealActivity.this,
                R.anim.projectile_anim);

        projectile.startAnimation(bottomUp);
        projectile.setVisibility(View.GONE);

        bottomUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Commons.animateView((findViewById(R.id.btnClaims)), 1.2f, 80);
                fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsClaimed(), new FireBaseHandler.OnCampaignsRecieved() {
                    @Override
                    public void onCampaignsRecieved(List<Campaign> campaigns) {
                        setClaimCount(campaigns.size());

                    }
                });
                scrollView.fullScroll(ScrollView.FOCUS_UP);
                showWinnerDialog(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void setClaimCount(int count) {
        if (count < 1)
            findViewById(R.id.lytClaimCount).setVisibility(View.INVISIBLE);
        else {
            findViewById(R.id.lytClaimCount).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.txtClaimCount)).setText(String.valueOf(count));
        }
    }

    private void checkIfClaimed() {
        if (Account.winnersPerson.getCampaignsClaimed() != null) {
            setClaimed(isClaimed());
        }
    }

    private boolean isClaimed() {
        return Account.winnersPerson.getCampaignsClaimed().contains(campaignId);
    }

    private void checkIfFavourite(String campaignId) {
        if (Account.winnersPerson.getCampaignsFavourites() != null) {
            if (Account.winnersPerson.getCampaignsFavourites().contains(campaignId)) {
                setFavourited(true);
            } else
                setFavourited(false);
        }
    }

    private void showWinnerDialog(boolean show) {
        if (show) {
            Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
            findViewById(R.id.lytWinnerDialog).startAnimation(fadeInAnimation);
            fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    findViewById(R.id.lytWinnerDialog).setVisibility(View.VISIBLE);
                    Commons.animateView((findViewById(R.id.lytWinnerDialog)), 1.1f, 80);
                    CommonConfetti.rainingConfetti(((RelativeLayout) findViewById(R.id.lytWinnerDialog)),
                            new int[]{Color.parseColor("#fecd3f"), Color.parseColor("#adbc7a"), Color.parseColor("#1ba4e4"), Color.parseColor("#bb3236")})
                            .oneShot();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            Animation fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
            findViewById(R.id.lytWinnerDialog).startAnimation(fadeOutAnimation);
            fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    findViewById(R.id.lytWinnerDialog).setVisibility(View.INVISIBLE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    private void shareClick() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                "Hey, you may be interested in this Deal! \n\n" +
                        Html.fromHtml(thisCampaign.getCampaignName()) +
                        "\n\nDownload the EPFS Winners App from the play store through this link:\n" +
                        "https://play.google.com/store/apps/details?id=com.epfs.hammock.winners&hl=en");
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }

//    private void callClick(final String phoneNumber) {
//        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog))
//                .setMessage("Do you want to call " + thisCampaign.getVendorName() + " on " + phoneNumber + "?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String uri = "tel:" + phoneNumber.trim();
//                        Intent intent = new Intent(Intent.ACTION_CALL);
//                        intent.setData(Uri.parse(uri));
//                        startActivity(intent);
//                    }
//                }).setNegativeButton("No", null)
//                .show();
//
//
//    }

    private void vendorClicked() {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra("merchantId", thisCampaign.getVendorId());
        intent.putExtra("merchantName", thisCampaign.getVendorName());
        startActivity(intent);
    }

    private void setDistance(boolean isGpsOn) {
        if (isGpsOn) {
            locationManager = (LocationManager) getSystemService
                    (Context.LOCATION_SERVICE);

            Criteria criteria = new Criteria();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestSingleUpdate(criteria, new android.location.LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    try {
                        ((TextView) findViewById(R.id.lblDistance)).setText(String.format("%.2f", Commons.CalculationByDistance(
                                new LatLng(location.getLatitude(), location.getLongitude()),
                                new LatLng(thisCampaign.getLat(), thisCampaign.getLng()))) + " km");
                    } catch (Exception ex) {
                        Log.d("onLocationChange", "onLocationChanged: " + ex.toString());
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            }, null);
        } else {
            findViewById(R.id.lytDistance).setVisibility(View.GONE);
        }
    }


    @Override
    public void onCampaignsRecieved(List<Campaign> campaigns) {
        setImageHeight();
        try {
            setCampaignItem(campaigns.get(0), false);

        } catch (Exception ex) {
            Log.d(TAG, "onCampaignsRecieved: " + ex.getMessage());
        }
    }

    @Override
    public void onFavourited(boolean favourite) {
        setFavourited(favourite);
    }

    @Override
    public void onClaimed(Campaign campaign, boolean updateClaimed) {
        if (campaign != null) {
            try {
                setCampaignItem(campaign, updateClaimed);

            } catch (Exception ex) {
                Log.d(TAG, "onCampaignsRecieved: " + ex.getMessage());
            }
        } else
            claimVoucher();
    }
}

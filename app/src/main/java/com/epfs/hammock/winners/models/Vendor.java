package com.epfs.hammock.winners.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by allan on 6/15/17.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vendor {
    private String vendorName;
    private String vendorImageUrl;
    private String vendorId;
}

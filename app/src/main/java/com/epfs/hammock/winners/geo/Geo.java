package com.epfs.hammock.winners.geo;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.epfs.hammock.winners.models.Campaign;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 6/29/17.
 */

public class Geo implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status> {

    Context context;

    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 12 * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 300;

    public ArrayList<Geofence> mGeofenceList;
    public GoogleApiClient mGoogleApiClient;

    List<Campaign> campaigns = new ArrayList<>();

    String TAG = "mGoogleApiClient";

    public Geo(Context context, List<Campaign> campaigns) {
        this.context = context;
        this.campaigns = campaigns;

        if (campaigns.size() > 0) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mGeofenceList = new ArrayList<>();
        populateGeofenceList();
        enableGeoFence();
        Log.d("mGoogleApiClient", "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("mGoogleApiClient", connectionResult.getErrorMessage());

    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Geofences added!");
        } else {
            Log.d(TAG, "Geofences NOT added!");
        }
    }


    public void populateGeofenceList() {
        for (Campaign campaign : campaigns) {
            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(campaign.getCampaignId())
                    .setCircularRegion(
                            campaign.getLat(),
                            campaign.getLng(),
                            Geo.GEOFENCE_RADIUS_IN_METERS
                    )
                    .setExpirationDuration(Geo.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build());
        }
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void enableGeoFence() {
        if (!mGoogleApiClient.isConnected()) {
            Toast.makeText(context, "Google API Client not connected!", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        try {
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
            builder.addGeofences(mGeofenceList);
            return builder.build();
        } catch (Exception ex) {
            return null;
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void disconnectGeoFence() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.disconnect();
            }
        }
    }
}

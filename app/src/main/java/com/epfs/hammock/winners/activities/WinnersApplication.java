package com.epfs.hammock.winners.activities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by allan on 8/17/17.
 */

public class WinnersApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Lato-Thin.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build());

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

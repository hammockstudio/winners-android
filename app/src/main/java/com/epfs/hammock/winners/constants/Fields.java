package com.epfs.hammock.winners.constants;

/**
 * Created by allan on 6/23/17.
 */

public class Fields {
    public static String campaigns = "campaigns";
    public static String users = "users";
    public static String categories = "categories";
    public static String vendors = "vendors";
    public static String slider = "slider";
    public static String appversion = "appversion";

    public static String numberOfVouchersSold = "numberOfVouchersSold";
    public static String remainingVouchers = "remainingVouchers";
}

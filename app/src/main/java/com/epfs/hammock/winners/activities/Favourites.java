package com.epfs.hammock.winners.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.epfs.hammock.winners.R;

import com.epfs.hammock.winners.adapters.FavouritesCampaignsAdapter;
import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.epfs.hammock.winners.models.Campaign;

import java.util.ArrayList;
import java.util.List;

public class Favourites extends AppCompatActivity implements FavouritesCampaignsAdapter.RecyclerViewClickListener,
        FireBaseHandler.OnCampaignsRecieved, FireBaseHandler.DealUpdate {

    RecyclerView listClaimedVouchers;

    FavouritesCampaignsAdapter favouritesCampaignsAdapter;

    FireBaseHandler fireBaseHandler;

    ProgressDialog progressDialog;

    List<Campaign> validCampaigns = new ArrayList<>();

    int selectedCampaignIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claimed_campaigns);
        fireBaseHandler = new FireBaseHandler(this);
        progressDialog = new ProgressDialog(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.favourites);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog.setMessage("Getting your favourites..");
        progressDialog.show();
        listClaimedVouchers = (RecyclerView) findViewById(R.id.listClaimedVouchers);
        registerForContextMenu(listClaimedVouchers);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listClaimedVouchers.setLayoutManager(mLayoutManager);
        listClaimedVouchers.setItemAnimator(new DefaultItemAnimator());
        favouritesCampaignsAdapter = new FavouritesCampaignsAdapter(validCampaigns, this, this);
        listClaimedVouchers.setAdapter(favouritesCampaignsAdapter);


    }

    @Override
    public void onResume(){
        super.onResume();
        fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsFavourites(), this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.listClaimedVouchers) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.list_longpress, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete:
                progressDialog.setMessage("Removing..");
                progressDialog.show();
                fireBaseHandler.switchFavourite(favouritesCampaignsAdapter.getData().get(selectedCampaignIndex)
                        .getCampaignId(), Account.winnersUser.getUid(), false, this);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(Favourites.this, DealActivity.class);
        intent.putExtra("campaignId", validCampaigns.get(position).getCampaignId());
        startActivity(intent);
    }

    @Override
    public void recyclerViewListLongClicked(View v, int position) {
        selectedCampaignIndex = position;
        openContextMenu(v);
    }

    @Override
    public void onCampaignsRecieved(List<Campaign> campaigns) {
        progressDialog.dismiss();
        this.validCampaigns = campaigns;

        if (validCampaigns.size() > 0) {
//            ((TextView) findViewById(R.id.txtDesc)).setText("You have " + validCampaigns.size() + " favourites");
            ((TextView) findViewById(R.id.txtInst)).setVisibility(View.VISIBLE);

        } else {
            ((TextView) findViewById(R.id.txtDesc)).setText("You do not have any favourites");
            ((TextView) findViewById(R.id.txtDesc)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.txtInst)).setVisibility(View.GONE);

        }
//        (findViewById(R.id.txtDesc)).setVisibility(View.VISIBLE);
        favouritesCampaignsAdapter.setData(validCampaigns);
        favouritesCampaignsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFavourited(boolean favourite) {
        progressDialog.setMessage("Getting your favourites..");
        progressDialog.show();
        fireBaseHandler.getCampaignByIds(Account.winnersPerson.getCampaignsFavourites(), this);

    }

    @Override
    public void onClaimed(Campaign campaign, boolean updateClaimed) {

    }
}


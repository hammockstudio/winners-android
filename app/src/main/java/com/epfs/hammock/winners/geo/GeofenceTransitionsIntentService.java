package com.epfs.hammock.winners.geo;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.epfs.hammock.winners.constants.Account;
import com.epfs.hammock.winners.data.FireBaseHandler;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 6/29/17.
 */

public class GeofenceTransitionsIntentService extends IntentService{
    protected static final String TAG = "GeofenceTransitionsIS";
    FireBaseHandler fireBaseHandler = new FireBaseHandler(this);

    public GeofenceTransitionsIntentService() {
        super(TAG);  // use TAG to name the IntentService worker thread
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()) {
            Log.e(TAG, "GeofencingEvent Error: " + event.getErrorCode());
            return;
        }
        getGeofenceTransitionDetails(event);
    }

    private void getGeofenceTransitionDetails(GeofencingEvent event) {
        List<String> triggeringIDs = new ArrayList<String>();
        for (Geofence geofence : event.getTriggeringGeofences()) {
            triggeringIDs.add(geofence.getRequestId());
        }
        GeoListener.setGeoNearby(triggeringIDs);
//        fireBaseHandler.getCampaignByIds(triggeringIDs, this);
//        fireBaseHandler.setCampaignsSeen(triggeringIDs, Account.winnersPerson.getPersonId());
    }

//    private void setNotification(String[] notificationMessage) {
//        int requestID = (int) System.currentTimeMillis();
//        Uri alarmSound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationManager mNotificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
//        Intent notificationIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
//        notificationIntent.putExtra("notificationCampaignId",notificationMessage[2]);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
//                .setSmallIcon(R.drawable.winnerlogoonly)
//                .setContentTitle(notificationMessage[0])
//                .setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText(notificationMessage[1]))
//                .setContentText(notificationMessage[1]).setAutoCancel(true);
//        mBuilder.setSound(alarmSound);
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(9, mBuilder.build());
//    }


//    @Override
//    public void onCampaignsRecieved(List<Campaign> campaigns) {
//        if(campaigns.size()>0) {
//            String[] notification = new String[3];
//            if (campaigns.size() > 1) {
//                notification[0] = "There are " + campaigns.size() + " campaigns nearby";
//                notification[1] = campaigns.get(0).getCampaignName();
//                notification[2] = campaigns.get(0).getCampaignId();
//
//            } else {
//                notification[0] = "There is 1 campaign nearby";
//                notification[1] = campaigns.get(0).getCampaignName();
//                notification[2] = campaigns.get(0).getCampaignId();
//            }
//            setNotification(notification);
//        }
//    }
}
